# GPU Path Tracer


![picture alt](Wideangle-Render.png)
*Wide Angle Lens Render*

Scalable GPU Path tracer written in CUDA.  Currently a bit of a work in progress but at the moment exists as a stand alone application.
Requires CUDA 10 and Visual Studio 2017 in order to build, all other dependencies are included in the project. 

Features include:

* Horizontally scalable across multiple GPUs.
* High-dynamic-range rendering with reinhard tonemapping.
* Random Direct lighting estimation.
* Framework for flexible on GPU material branching.
* Lambertian BRDF with importance sampling for Wi ray selection.
* Realistic camera model, some aspects of this have been adapted from implementation in [pbrt project](https://www.pbrt.org/) (Focus finding and exit pupil sampling).
* Naive Pinhole camera model.
* Asset importing with open assimp.

![picture alt](Fisheye-Render.png)
*Fisheye Lens Render*

![picture alt](Pinhole.png)
*Pinhole Camera Render*

TODO:

* Normal mapping.
* Multiple material types. Glossy, reflective etc.
* Material sorting
* Improve ray scheduling to not waste gpu cores.

