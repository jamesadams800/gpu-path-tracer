#ifndef CRT_TEXTURE_H_
#define CRT_TEXTURE_H_

#include <stdint.h>
#include <vector>
#include <memory>
#include <functional>

#include <glm\glm.hpp>

namespace pathtracer {

class TextureLoadError : public std::runtime_error {
public:
  TextureLoadError(const std::string& what)
     : std::runtime_error(what) {
  }
private:
};

template <class T>
using ImagePtr = std::unique_ptr<T, std::function<void(T*)>>;

class Texture {
public:
  Texture();
  Texture(const char * file_name);
  Texture(const glm::vec3& color);
  ~Texture();

  Texture(const Texture&) = delete;
  Texture& operator=(const Texture&) = delete;

  void LoadFile(const char * file_name);
  void LoadUniformColor(const glm::vec3 & color);

  uint32_t GetChannels() const {
    return channels_;
  }

  uint32_t GetWidth() const {
    return width_;
  }

  uint32_t GetHeight() const {
    return height_;
  }

  const float* GetData() const {
    return  texture_data_.get();
  }

private:
  ImagePtr<float> texture_data_;

  int32_t channels_;
  int32_t width_;
  int32_t height_;
};

} //namespace pathtracer

#endif // CRT_TEXTURE_H_