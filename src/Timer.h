#ifndef CRT_TIMER_H_
#define CRT_TIMER_H_

#include <chrono>

namespace pathtracer {

class Timer {
public:
  Timer()
    : time_duration_{},
      start_{} {
  }

  void Start() {
    start_ = std::chrono::high_resolution_clock::now();
  }

  void Stop() {
    time_duration_ = std::chrono::high_resolution_clock::now() - start_;
  }

  float GetTimeMs() {
    time_duration_ = std::chrono::high_resolution_clock::now() - start_;
    std::chrono::microseconds time =
      std::chrono::duration_cast<std::chrono::microseconds>(time_duration_);
    return time.count() / 1000.0f;
  }

  float GetTimeS() {
    time_duration_ = std::chrono::high_resolution_clock::now() - start_;
    std::chrono::milliseconds time =
      std::chrono::duration_cast<std::chrono::milliseconds>(time_duration_);
    return time.count() / 1000.0f;
  }

private:
  std::chrono::duration<float> time_duration_;
  std::chrono::steady_clock::time_point start_;
};

} // namespace pathtracer
#endif // CRT_TIMER_H_