#include "RenderDevice.h"

#include <chrono>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "CudaException.h"
#include "DeviceRng.h"
#include "DeviceScene.h"

namespace pathtracer {

RenderDevice::RenderDevice(int32_t device_index)
  : device_index_{ device_index },
    device_reset_{ false },
    properties_{},
    device_scene_{ nullptr } {
  this->Bind();
  CHECK_CUDA_ERROR(cudaGetDeviceProperties(&properties_, device_index_));
  // Delay instantiation of DeviceScene Object until bind has been called.
  device_scene_.reset(new DeviceScene(*this));
  device_rng_.reset(new DeviceRng(*this));
}

RenderDevice::~RenderDevice() {
}

void RenderDevice::Bind() {
  CHECK_CUDA_ERROR(cudaSetDevice(device_index_));
}

void RenderDevice::DisplayDeviceProperties() const {
  std::cout << "-----------------------------------------------------" << std::endl;
  std::cout << "| Displaying Cuda Device Properties                  |" << std::endl;
  std::cout << "-----------------------------------------------------" << std::endl;
  std::cout << "| Name: " << std::left << std::setw(45) << properties_.name
            << "|" << std::endl;
  std::cout << "| Max Threads per block: " << std::left << std::setw(28)
            << properties_.maxThreadsPerBlock << "|" << std::endl;
  auto grid_size = properties_.maxGridSize;
  std::cout << "| Max Grid size: " << grid_size[0] << "x" << grid_size[1] << "x"
            << grid_size[2] << std::left << std::setw(45) << "|" << std::endl;
  std::cout << "-----------------------------------------------------" << std::endl;
}

void RenderDevice::CopyToDevice(const Scene& scene) {
  this->Bind();
  scene.CopyToDevice(*this);
  // Rebuild descriptors.
  this->GetScene().GetGeometry().FinaliseGeometry();
}

} // namespace pathtracer;
