#ifndef _CRT_INDEXBUFFER_H_
#define _CRT_INDEXBUFFER_H_

#include <memory>
#include <vector>
#include <map>

namespace pathtracer {

class IndexBuffer {
public:
  IndexBuffer();

  void AddIndexData(uint32_t *data, size_t size);
  void AddIndexData(const std::vector<uint32_t>& vector);

  inline uint32_t operator[](uint32_t index) const noexcept {
    return data_[index];
  }

  inline size_t Size() const noexcept { 
    return data_.size();
  }
private:
  std::vector<uint32_t> data_;
};

} // namespace pathtracer

#endif // _CRT_INDEXBUFFER_H_