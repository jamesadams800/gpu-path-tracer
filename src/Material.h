#ifndef CRT_MATERIAL_H_
#define CRT_MATERIAL_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include <glm\glm.hpp>

#include "IDeviceCopyable.h"
#include "Texture.h"

namespace pathtracer {

class Material : public IDeviceCopyable {
public:
  Material();

  enum TextureType {
    ALBEDO,
    SPECULAR,
    EMISSIVE,
    SHININESS,
    MAX_TEXTURE
  };

  uint32_t GetUuid() const {
    return uuid_;
  }

  bool IsEmissive() const {
    return is_emissive_;
  }

  void SetAlbedo(float r, float g, float b);
  void SetSpecular(float r, float g, float b);
  void SetEmissive(float r, float g, float b);
  void SetShininess(float s);

  glm::vec3 GetAlbedo() const {
    return albedo_;
  }
  glm::vec3 GetSpecular() const {
    return specular_;
  }
  glm::vec3 GetEmissive() const {
    return emissive_;
  }
  float GetShininess() const {
    return shininess_;
  }

  void SetTexture(std::unique_ptr<Texture> texture, TextureType);
  const Texture* GetTexture(TextureType type) const {
    return textures_[type].get();
  }

  void CopyToDevice(RenderDevice& device) const override;

private:
  uint32_t uuid_;
  static uint32_t uuid_counter_;
  glm::vec3 albedo_;
  glm::vec3 specular_;
  glm::vec3 emissive_;
  bool is_emissive_;
  float shininess_;
  std::unique_ptr<Texture> textures_[MAX_TEXTURE];
};

} // namespace pathtracer 

#endif // CRT_MATERIAL_H_