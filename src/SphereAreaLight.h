#ifndef CRT_SPHERICALAREALIGHT_H_
#define CRT_SPHERICALAREALIGHT_H_

#include <stdint.h>
#include <vector>
#include <memory>
#include <array>

#include "Light.h"

namespace pathtracer {

class SphereAreaLight : public Light {
public:
  explicit SphereAreaLight(float radius,
                           const glm::vec3& color, 
                           float intensity = 1.0f);
  explicit SphereAreaLight(const glm::vec3& color, float intensity = 1.0f);

  float GetRadius() const {
    return radius_;
  }

protected:
  float radius_;
};

} // namespace pathtracer 

#endif // CRT_SPHERICALAREALIGHT_H_