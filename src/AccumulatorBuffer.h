#ifndef _CRT_ACCUMULATORBUFFER_H_
#define _CRT_ACCUMULATORBUFFER_H_

#include "RenderTarget.h"
#include "Framebuffer.h"

namespace pathtracer {

class AccumulatorBuffer : public RenderTarget {
public:
  struct Pixel {
    glm::vec3 color = {0.0f,0.0f,0.0f};
    float depth = FLT_MAX;
    uint32_t samples = 0;
  };

  explicit AccumulatorBuffer(uint32_t width, uint32_t height);
  AccumulatorBuffer(const AccumulatorBuffer&) = delete;
  AccumulatorBuffer& operator=(const AccumulatorBuffer&) = delete;

  void HandleSample(const Sample& sample) override;
  void WriteFramebuffer(Framebuffer& framebuffer);

private:
  Framebuffer::Pixel GeneratePixel(const AccumulatorBuffer::Pixel & accumulator_pixel);
  glm::vec3 ReinhardToneMap(const glm::vec3& in_color);
  glm::vec3 GammaCorrect(const glm::vec3& in_color);

  uint32_t x_res_;
  uint32_t y_res_;
  std::unique_ptr<Pixel[]> data_;
};

} // namespace pathtracer

#endif // _CRT_ACCUMULATORBUFFER_H_