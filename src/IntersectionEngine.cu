#include "IntersectionEngine.h"

namespace pathtracer {

__device__ bool IntersectionEngine::TriangleIntersect(const device::Ray& ray,
                                                      const glm::vec3 & v0,
                                                      const glm::vec3 & v1,
                                                      const glm::vec3 & v2,
                                                      float& t,
                                                      glm::vec3& barycentric) {
  glm::vec3 edge1 = v1 - v0;
  glm::vec3 edge2 = v2 - v0;
  glm::vec3 pvec = glm::cross(ray.GetDirection(), edge2);

  float det = glm::dot(edge1, pvec);
  if (det < FLT_EPSILON) {
    return false;
  }

  glm::vec3 tvec = ray.GetOrigin() - v0;
  float u = glm::dot(tvec, pvec);
  if (u < 0 || u > det) {
    return false;
  }

  glm::vec3 qvec = glm::cross(tvec, edge1);
  float v = glm::dot(ray.GetDirection(), qvec);
  if (v < 0 || u + v > det) {
    return false;
  }
  // t = intersection point in terms of O + tD where O is ray origin and D is ray direction.
  t = glm::dot(edge2, qvec);
  float inv_det = 1.0f / det;
  t = t * inv_det;
  // u and v are barycentric coordinates of intersection on triangle.
  u *= inv_det;
  v *= inv_det;

  barycentric.x = (1 - u - v);
  barycentric.y = u;
  barycentric.z = v;

  return true;
}

__device__ bool IntersectionEngine::QuadIntersect(const device::Ray & ray,
                                                  const device::Quad& quad,
                                                  float & t,
                                                  glm::vec2 & bilinear_coords) {

  const glm::vec3& v00 = quad.v0;
  const glm::vec3& v10 = quad.v3;
  const glm::vec3& v11 = quad.v2;
  const glm::vec3& v01 = quad.v1;

  // 1. Check for intersection on quad.
  glm::vec3 e01 = v10 - v00;
  glm::vec3 e03 = v01 - v00;
  glm::vec3 pvec = glm::cross(ray.GetDirection(), e03);
  float det = glm::dot(e01, pvec);
  if (fabs(det) < FLT_EPSILON) {
    return false;
  }
  float inv_det = 1.0f / det;
  glm::vec3 tvec = ray.GetOrigin() - v00;
  float alpha = glm::dot(tvec, pvec) * inv_det;
  if (alpha < 0.0f) {
    return false;
  }
  if (alpha > 1.0f) {
    return false;
  }
  glm::vec3 qvec = glm::cross(tvec, e01);
  float beta = glm::dot(ray.GetDirection(), qvec) * inv_det;
  if (beta < 0.0f) {
    return false;
  }
  if (beta > 1.0f) {
    return false;
  }

  if ((alpha + beta) > 1.0f) {
    glm::vec3 e23 = v01 - v11;
    glm::vec3 e21 = v10 - v11;
    glm::vec3 pvecp = glm::cross(ray.GetDirection(), e21);
    float det_prime = glm::dot(e23, pvecp);
    if (fabsf(det_prime) < FLT_EPSILON) {
      return false;
    }
    float inv_det_prime = 1.0f / det_prime;
    glm::vec3 tvecp = ray.GetOrigin() - v11;
    float alpha_prime = glm::dot(tvecp, pvecp) * inv_det_prime;
    if (alpha_prime < 0.0f) {
      return false;
    }
    glm::vec3 qvecp = glm::cross(tvecp, e23);
    float beta_prime = glm::dot(ray.GetDirection(), qvecp) * inv_det_prime;
    if (beta_prime < 0) {
      return false;
    }
  }

  // 2. Compute ray parameters of intersection.
  t = glm::dot(e03, qvec) * inv_det;
  if (t < 0.0f) {
    return false;
  }

  glm::vec3 e02 = v11 - v00;
  glm::vec3 nvec = glm::cross(e01, e03);
  glm::vec3 nvecabs = glm::abs(nvec);
  float alpha11;
  float beta11;
  if (nvecabs.x >= nvecabs.y && nvecabs.x >= nvecabs.z) {
    float nx = 1.0f / nvec.x;
    alpha11 = (e02.y * e03.z - e02.z * e03.y) * nx;
    beta11 = (e01.y * e02.z - e01.z * e02.y) * nx;
  }
  else if (nvecabs.y >= nvec.x && nvecabs.y >= nvecabs.z) {
    float ny = 1.0f / nvec.y;
    alpha11 = (e02.z * e03.x - e02.x * e03.z) * ny;
    beta11 = (e01.z * e02.x - e01.x * e02.z) * ny;
  }
  else {
    float nz = 1.0f / nvec.z;
    alpha11 = (e02.x * e03.y - e02.y * e03.x) * nz;
    beta11 = (e01.x * e02.y - e01.y * e02.x) * nz;
  }

  // 3. Compute bilinear coordinates.
  if (fabsf(alpha11 - 1.0f) < FLT_EPSILON) {
    bilinear_coords.x = alpha;
    if (fabsf(beta11 - 1.0f) < FLT_EPSILON) {
      bilinear_coords.y = beta;
    }
    else {
      bilinear_coords.y = beta / (alpha * (beta11 - 1.0f) + 1.0f);
    }
  }
  else if (fabsf(beta11 - 1.0f) < FLT_EPSILON) {
    bilinear_coords.y = beta;
    bilinear_coords.x = alpha / (beta * (alpha11 - 1.0f) + 1.0f);
  }
  else {
    float a = -(beta11 - 1.0f);
    float b = alpha * (beta11 - 1.0f) - beta * (alpha11 - 1.0f) - 1.0f;
    float c = alpha;
    float delta = (b * b) - 4.0f * a * c;
    float q = -1.0f / 2 * (b + glm::sign(b) * sqrt(delta));
    bilinear_coords.x = q / a;
    if (bilinear_coords.y < 0.0f || bilinear_coords.y > 1.0f) {
      bilinear_coords.x = c / q;
    }
    bilinear_coords.y = beta / (bilinear_coords.y * (beta11 - 1.0f) + 1.0f);
  }

  return true;
}

__device__ bool IntersectionEngine::SphereIntersect(const device::Ray& ray,
                                                    const device::Sphere& sphere,
                                                    device::Intersection& intersection) {

  glm::vec3 l = sphere.position - ray.GetOrigin();
  float tca = glm::dot(l, ray.GetDirection());
  if (tca < 0) {
    return false;
  }
  float d2 = glm::dot(l, l) - tca * tca;
  if (d2 > sphere.radius * sphere.radius) {
    return false;
  }
  float thc = sqrt(sphere.radius * sphere.radius - d2);
  if (tca - thc < intersection.t) {
    intersection.has_hit = true;
    intersection.t = tca - thc;
    intersection.position = ray.GetOrigin() + (ray.GetDirection() * intersection.t);
    intersection.normal = glm::normalize(intersection.position - sphere.position);
    intersection.uv = glm::vec2(atan2f(intersection.position.z, intersection.position.x),
                                intersection.position.y / sphere.radius);
    intersection.brdf = sphere.brdf;
  }
  return true;
}

} // namespace pathtracer
