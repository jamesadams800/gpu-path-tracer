#include "UniformSampleGenerator.h"

#include "RenderDevice.h"
#include "SampleScheduler.h"
#include "RenderTarget.h"
#include "DeviceArray.h"
#include "CudaException.h"

namespace pathtracer {

__global__ void UniformSampleKernel(SampleSpace sample_space,
                                    device::Array<Sample> samples) {
  uint32_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if (kernel_index < samples.size) {
    samples.data[kernel_index].ss_cord = UniformSampleGenerator::IndexToNormSSCoord(sample_space,
                                                                                    kernel_index);
    samples.data[kernel_index].depth = FLT_MAX;
    samples.data[kernel_index].color = glm::vec4{ 0.0f, 0.0f, 0.0f, 0.0f };
  }
}

UniformSampleGenerator::UniformSampleGenerator(RenderDevice & device)
  : DeviceRunnable{ device } {
}

uint32_t UniformSampleGenerator::GenerateSamples(const SampleSpace & input,
                                                 thrust::device_vector<Sample>& out_samples) {
  uint32_t number_of_samples = (input.end_x - input.start_x) *
                               (input.end_y - input.start_y) *
                               pow(input.pixel_ss_kernel_size, 2);
  if (out_samples.size() != number_of_samples) {
    out_samples.resize(number_of_samples);
  }
  auto config = this->GetDevice().GetLaunchConfig(number_of_samples);
  UniformSampleKernel<<<config.first, config.second>>>(input, device::ToArray(out_samples));

  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
  return number_of_samples;
}

__device__ glm::vec2 UniformSampleGenerator::IndexToNormSSCoord(const SampleSpace& input,
                                                                uint32_t kernel_index) {
  glm::vec2 normalised_ss_coord;

  uint32_t index = kernel_index;
  uint32_t x_ss_index = index % input.pixel_ss_kernel_size;
  index = index / input.pixel_ss_kernel_size;

  uint32_t y_ss_index = index % input.pixel_ss_kernel_size;
  index = index / input.pixel_ss_kernel_size;

  uint32_t x_pixel_index = index % (input.end_x - input.start_x);
  uint32_t y_pixel_index = index / (input.end_x - input.start_x);

  // Add 1 index and kernel size to centre the sample kernel over the pixel.
  float normalised_ss_x = static_cast<float>(x_ss_index + 1) / static_cast<float>(input.pixel_ss_kernel_size + 1);
  float normalised_ss_y = static_cast<float>(y_ss_index + 1) / static_cast<float>(input.pixel_ss_kernel_size + 1);

  float pixel_x_with_ss_offset = input.start_x + x_pixel_index + normalised_ss_x;
  float pixel_y_with_ss_offset = input.start_y + y_pixel_index + normalised_ss_y;

  normalised_ss_coord.x = pixel_x_with_ss_offset / static_cast<float>(input.res_x);
  normalised_ss_coord.y = pixel_y_with_ss_offset / static_cast<float>(input.res_y);

  return normalised_ss_coord;
}

} // namespace pathtracer
