#include "AccumulatorThread.h"

#include <iostream>
#include <assert.h>

#include "Framebuffer.h"
#include "AccumulatorBuffer.h"
#include "ILogger.h"

namespace pathtracer {

AccumulatorThread::AccumulatorThread(ILogger& logger, const Framebuffer& framebuffer)
  : EventThread(logger),
    exit_{false},
    buffer_{ new AccumulatorBuffer(framebuffer.GetWidth(), framebuffer.GetHeight()) } {
}

void AccumulatorThread::SendSamples(std::unique_ptr<thrust::host_vector<Sample>> samples) {
  assert(samples);
  auto event = std::make_unique<ThreadEvent>(ThreadEvent::ACCUMULATOR_ADD_SAMPLE_REQ);
  event->add_sample.samples = std::move(samples);
  this->SendEvent(std::move(event));
}

void AccumulatorThread::SendWriteFramebuffer(Framebuffer* framebuffer) {
  assert(framebuffer);
  auto event = std::make_unique<ThreadEvent>(ThreadEvent::ACCUMULATOR_WRITE_FRAMEBUFFER);
  event->write_frame.framebuffer = framebuffer;
  this->SendEvent(std::move(event));
}

void AccumulatorThread::ThreadMain() {
  Logger().Log("AccumulatorThread::ThreadMain() :" + std::to_string(this->GetId()));
  while (!exit_) {
    this->CheckThreadEvents(true);
  }
}

void AccumulatorThread::CheckThreadEvents(bool blocking) {
  std::unique_ptr<ThreadEvent> event(this->GetNextEvent(blocking));
  if (event) {
    switch (event->id) {
      case ThreadEvent::THREAD_INIT_REQ: {
        Logger().Log("THREAD_INIT_REQ: Recieved in thread " + std::to_string(this->GetId()));
        break;
      }
      case ThreadEvent::THREAD_TERMINATE_REQ: {
        Logger().Log("THREAD_TERMINATE_REQ: Recieved in thread " + std::to_string(this->GetId()));
        exit_ = true;
        break;
      }
      case ThreadEvent::ACCUMULATOR_ADD_SAMPLE_REQ: {
        ThreadEvent::AccumulatorAddSample& add_sample = event->add_sample;
        // Add sample vector to unique ptr so resource is destroyed once finished.
        std::unique_ptr<thrust::host_vector<Sample>> samples(std::move(add_sample.samples));
        add_sample.samples = nullptr;
        this->AccumulateSamples(*samples);
        break;
      }
      case ThreadEvent::ACCUMULATOR_WRITE_FRAMEBUFFER: {
        Logger().Log("ACCUMULATOR_WRITE_FRAMEBUFFER: Recieved in thread " + std::to_string(this->GetId()));
        ThreadEvent::AccumulatorWriteFrame& write_frame = event->write_frame;
        Framebuffer* framebuffer = write_frame.framebuffer;
        buffer_->WriteFramebuffer(*framebuffer);
        break;
      }
      default: {
        Logger().Log("Unknown Event [" + std::to_string(event->id) + "] recieved in thread "
          + std::to_string(this->GetId()), ILogger::ERROR);
        break;
      }
    }
  }
}

void AccumulatorThread::AccumulateSamples(const thrust::host_vector<Sample>& samples) {
  for (auto& sample : samples) {
    buffer_->HandleSample(sample);
  }
}

} // namespace pathtracer 
