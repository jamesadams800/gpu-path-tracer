#include "DeviceGeometry.h"

#include "CudaException.h"
#include "RenderDevice.h"
#include "DeviceScene.h"

namespace pathtracer {

__global__
void TransformKernel(device::Mesh* mesh,
                     glm::mat4 transform,
                     glm::mat4 normal_matrix,
                     size_t size) {
  uint32_t index = blockIdx.x * blockDim.x + threadIdx.x;
  uint32_t vertex_width = mesh->vertex_width[VertexBuffer::POSITION];
  device::Array<float> position = mesh->vertex_data[VertexBuffer::POSITION];
  if (index < size) {
    float& x = position.data[vertex_width * index];
    float& y = position.data[vertex_width * index + 1];
    float& z = position.data[vertex_width * index + 2];
    glm::vec4 p(x, y, z, 1.0f);
    p = transform * p;
    x = p.x;
    y = p.y;
    z = p.z;

    vertex_width = mesh->vertex_width[VertexBuffer::NORMAL];
    device::Array<float> normal = mesh->vertex_data[VertexBuffer::NORMAL];
    if (normal.size == position.size) {
      float& x = normal.data[vertex_width * index];
      float& y = normal.data[vertex_width * index + 1];
      float& z = normal.data[vertex_width * index + 2];
      glm::vec4 p(x, y, z, 1.0f);
      p = normal_matrix * p;
      x = p.x;
      y = p.y;
      z = p.z;
    }

    vertex_width = mesh->vertex_width[VertexBuffer::BITANGENT];
    device::Array<float> bitangent = mesh->vertex_data[VertexBuffer::BITANGENT];
    if (bitangent.size == position.size) {
      float& x = bitangent.data[vertex_width * index];
      float& y = bitangent.data[vertex_width * index + 1];
      float& z = bitangent.data[vertex_width * index + 2];
      glm::vec4 p(x, y, z, 1.0f);
      p = normal_matrix * p;
      x = p.x;
      y = p.y;
      z = p.z;
    }

    vertex_width = mesh->vertex_width[VertexBuffer::TANGENT];
    device::Array<float> tangent = mesh->vertex_data[VertexBuffer::TANGENT];
    if (tangent.size == position.size) {
      float& x = tangent.data[vertex_width * index];
      float& y = tangent.data[vertex_width * index + 1];
      float& z = tangent.data[vertex_width * index + 2];
      glm::vec4 p(x, y, z, 1.0f);
      p = normal_matrix * p;
      x = p.x;
      y = p.y;
      z = p.z;
    }
  }
}

DeviceGeometry::DeviceGeometry(RenderDevice& device)
  : device_{ device },
    device_geometry_{ thrust::device_malloc<device::Geometry>(1) },
    spheres_{},
    meshes_{},
    geometry_emitters_{ thrust::device_malloc<device::Geometry>(1) },
    mesh_emitters_{},
    sphere_emitters_{},
    vertex_data_{} {
}

DeviceGeometry::~DeviceGeometry() {
  thrust::device_free(device_geometry_);
  thrust::device_free(geometry_emitters_);
}

void DeviceGeometry::RebuildDescriptor() {
  device::Geometry descriptor = {0};
  descriptor.spheres = device::ToArray(spheres_);
  descriptor.meshes = device::ToArray(meshes_);
  device_geometry_[0] = descriptor;

  device::Geometry emissive = { 0 };
  emissive.spheres = device::ToArray(sphere_emitters_);
  emissive.meshes = device::ToArray(mesh_emitters_);
  geometry_emitters_[0] = emissive;
}

void DeviceGeometry::FinaliseGeometry() {
  this->BuildDeviceMeshes();
  this->RebuildDescriptor();
}

void DeviceGeometry::AddSphere(const Sphere &sphere) {
  device::Sphere device_sphere;
  device_sphere.position = sphere.GetWorldTransform() * sphere.GetCentre();
  device_sphere.radius = sphere.GetRadius();
  device_sphere.brdf = device_.GetScene().GetBrdfs()
                              .GetDeviceBrdf(sphere.GetMaterial()->GetUuid());
  spheres_.push_back(device_sphere);

  if (sphere.GetMaterial()->IsEmissive()) {
    sphere_emitters_.push_back(device_sphere);
  }
}

void DeviceGeometry::AddMesh(const Mesh &mesh) {
  const auto &vertex_buffer = *mesh.GetVertexBuffer();
  const auto &index_buffer = *mesh.GetIndexBuffer();
  MeshLoadData mesh_load = {};
  // Copy data to vertex data arrays.
  mesh_load.transform = mesh.GetWorldTransform();
  for (const auto &attribute : vertex_buffer.Attributes()) {
    size_t start_offset = vertex_data_[attribute.vertex_attribute].size();
    this->CopyAttribute(vertex_buffer,
                        index_buffer,
                        attribute.vertex_attribute,
                        attribute.vertex_size,
                        vertex_data_[attribute.vertex_attribute]);
    mesh_load.offsets[attribute.vertex_attribute] = std::make_pair(start_offset,
                                                    vertex_data_[attribute.vertex_attribute].size());
    mesh_load.attribute_width[attribute.vertex_attribute] = attribute.vertex_size;
  }
  mesh_load.vertex_count = index_buffer.Size();
  mesh_load.brdf = device_.GetScene().GetBrdfs()
                          .GetDeviceBrdf(mesh.GetMaterial()->GetUuid());
  mesh_load.is_emissive = mesh.GetMaterial()->IsEmissive();
  mesh_load_.push_back(mesh_load);
}

void DeviceGeometry::TransformMesh(device::Mesh* mesh, glm::mat4 transform, size_t size) {
  auto config = device_.GetLaunchConfig(size);
  TransformKernel<<<config.first, config.second>>>(mesh,
                                                   transform,
                                                   glm::inverse(glm::transpose(transform)),
                                                   size);
  CHECK_CUDA_ERROR(cudaGetLastError());
}

void DeviceGeometry::CopyAttribute(const VertexBuffer &vertex_buffer,
                                   const IndexBuffer &index_buffer,
                                   VertexBuffer::VertexAttribute attribute,
                                   uint32_t vertex_size,
                                   thrust::device_vector<float> &output) {
  thrust::host_vector<float> temp_buffer;
  temp_buffer.reserve(index_buffer.Size());
  for (uint32_t i = 0; i < index_buffer.Size(); i++) {
    uint32_t index = index_buffer[i];
    for (uint32_t j = 0; j < vertex_size; j++) {
      temp_buffer.push_back(vertex_buffer[attribute][index][j]);
    }
  }
  size_t back = output.size();
  output.resize(output.size() + temp_buffer.size());
  auto old_back = output.begin() + back;
  thrust::copy(temp_buffer.begin(),  temp_buffer.end(), old_back);
}

void DeviceGeometry::BuildDeviceMeshes() {
  for (auto load_data : mesh_load_) {
    device::Mesh device_mesh = { 0 };
    device_mesh.brdf = load_data.brdf;
    device_mesh.vertex_count = load_data.vertex_count;
    for (uint32_t i = 0; i < VertexBuffer::MAX_ATTRIBUTE_SIZE; i++) {
      device_mesh.vertex_data[i] = device::ArrayFromOffsets(vertex_data_[i],
                                                            load_data.offsets[i].first,
                                                            load_data.offsets[i].second);
      device_mesh.vertex_width[i] = load_data.attribute_width[i];
    }
    meshes_.push_back(device_mesh);
    if (load_data.is_emissive) {
      mesh_emitters_.push_back(device_mesh);
    }
    this->TransformMesh(thrust::raw_pointer_cast(&meshes_.back()),
                        load_data.transform,
                        load_data.vertex_count);
  }
}

} // namespace pathtracer
