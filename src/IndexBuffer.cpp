#include "IndexBuffer.h"

#include <cuda_runtime.h>

#include "CudaException.h"

namespace pathtracer {

IndexBuffer::IndexBuffer()
  : data_ {} {
}

void IndexBuffer::AddIndexData(const std::vector<uint32_t> &vector) { 
  data_ = vector; 
}

void IndexBuffer::AddIndexData(uint32_t *data, size_t size) {
  data_.resize(size);
  std::copy(data, data + size, data_.begin());
}

} // namespace pathtracer 
