#ifndef _CRT_CUDAEXCEPTION_H_
#define _CRT_CUDAEXCEPTION_H_

#include <string>

#include <cuda_runtime.h>

namespace pathtracer {

#define CHECK_CUDA_ERROR( error ) CheckError(error, __FUNCTION__, __LINE__) 

class CudaException : public std::runtime_error {
public:
  CudaException(cudaError_t error, const std::string & string)
    : std::runtime_error(string + " : " + std::string(cudaGetErrorString(error) ) +
                         "(" + std::to_string(error) + ")"  ) {
  }
private:
};

inline void CheckError(cudaError_t error, const char * func, uint32_t line) {
  if (error != cudaSuccess) {
    throw CudaException(error, std::string(func) + ":" + std::to_string(line));
  }
}

} // namespace pathtracer

#endif // _CRT_CUDAEXCEPTION_H_