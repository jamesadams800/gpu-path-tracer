#include "Framebuffer.h"

#include <assert.h>
#include <algorithm>

#include <cuda_runtime.h>

#include "CudaException.h"

namespace pathtracer {

Framebuffer::Framebuffer(uint32_t width, uint32_t height)
  : x_res_{ width },
    y_res_{ height },
    data_{ new Pixel[ width * height ] } {
}

void Framebuffer::Set(uint32_t x, uint32_t y, const Pixel& value) {
  assert(x < x_res_);
  assert(y < y_res_);
  uint32_t index = (y * x_res_) + x;
  data_[index] = value;
}

void Framebuffer::operator+=(const Framebuffer & other) {
  assert(other.x_res_ == x_res_ && other.y_res_ == y_res_, 
    " Framebuffer::operator+= : Attempted to add incompatible framebuffers.");
  for (uint32_t i = 0; i < x_res_ * y_res_; i++) {
    this->data_[i].color += other.data_[i].color;
    this->data_[i].depth = std::min( this->data_[i].depth, other.data_[i].depth);
  }
}

Framebuffer::Pixel Framebuffer::Get(uint32_t x, uint32_t y) const {
  assert(x < x_res_);
  assert(y < y_res_);
  uint32_t index = (y * x_res_) + x;
  return data_[index];
}

} // namespace pathtracer
