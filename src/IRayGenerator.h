#ifndef _CRT_IRAYGENERATOR_H_
#define _CRT_IRAYGENERATOR_H_

#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>

#include "Ray.h"
#include "DeviceCamera.h"
#include "DeviceBrdf.h"
#include "DeviceLight.h"
#include "Intersection.h"
#include "SampleScheduler.h"

namespace pathtracer {

struct Sample;

class IRayGenerator {
public:
  virtual ~IRayGenerator() {}
  virtual void GeneratePrimaryRays(const thrust::device_vector<Sample>& samples,
                                   const thrust::device_ptr<device::Camera>,
                                   thrust::device_vector<device::Ray>& primary_rays,
                                   thrust::device_vector<float>& weights) = 0;
  /*virtual thrust::device_vector<device::Ray> GenerateShadowRays(thrust::device_vector<device::Intersection>&,
                                                                thrust::device_ptr<device::Light>) = 0;
  virtual thrust::device_vector<device::Ray> GenerateIndirectRays(thrust::device_vector<device::Intersection>&,
                                                               thrust::device_vector<device::Material>&,
                                                               uint32_t n_samples = 1) = 0;*/
private:
};
} // namespace pathtracer

#endif // _CRT_IRAYGENERATOR_H_