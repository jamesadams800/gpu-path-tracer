#ifndef CRT_DEVICEPRIMITIVES_H_
#define CRT_DEVICEPRIMITIVES_H_

#include <glm/glm.hpp>

#include "VertexBuffer.h"
#include "DeviceArray.h"

namespace pathtracer {

namespace device {

class Brdf;

// v3 ----- v0
//  |        |
//  |        |
// v2 ----- v1
struct Quad {
  glm::vec3 v0;
  glm::vec3 v1;
  glm::vec3 v2;
  glm::vec3 v3;
};

struct DiffractiveEdge {
  // Consider geometry of the diffractive edge to be a quad. 
  // When a ray is intersected with the quad, if it passes close to
  // the edge then the ray will be diffracted.
  Quad surface;
  glm::vec3 dihedron_face0_normal;
  glm::vec3 dihedron_face1_normal;
};

struct Sphere {
  Sphere() = default;
  device::Brdf* brdf;
  glm::vec3 position;
  float radius;
};

struct Mesh {
  Mesh() = default;
  device::Brdf* brdf;
  size_t vertex_count;
  uint32_t vertex_width[VertexBuffer::MAX_ATTRIBUTE_SIZE];
  Array<float> vertex_data[VertexBuffer::MAX_ATTRIBUTE_SIZE];
};

} // namespace device

} // namespace pathtracer

#endif // CRT_DEVICEPRIMITIVES_H_