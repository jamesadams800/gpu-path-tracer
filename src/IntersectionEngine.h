#ifndef _CRT_INTERSECTIONENGINE_H_
#define _CRT_INTERSECTIONENGINE_H_

#include <thrust/device_vector.h>

#include "DevicePrimitives.h"
#include "DeviceInterp.h"
#include "VertexBuffer.h"
#include "Intersection.h"
#include "Ray.h"

namespace pathtracer {

class DeviceGeometry;

struct IntersectionSorter {
  __host__ __device__
  bool operator()(const device::Intersection& a, const device::Intersection& b) {
    if (a.has_hit && !b.has_hit) {
      return true;
    }
    return false;
  }
};

class IntersectionEngine {
public:
  virtual ~IntersectionEngine() {}

  virtual void Intersect(const thrust::device_vector<device::Ray>& rays,
                         const DeviceGeometry& geometry,
                         thrust::device_vector<device::Intersection>& ) = 0;

  // Triangle Intersection.
  __device__ static bool TriangleIntersect(const device::Ray& ray,
                                           const glm::vec3& v0,
                                           const glm::vec3& v1,
                                           const glm::vec3& v2,
                                           float& t,
                                           glm::vec3& barycentric);

  // Quad Intersection
  __device__ static bool QuadIntersect(const device::Ray& ray,
                                       const device::Quad& quad,
                                       float& t,
                                       glm::vec2& bilinear_coords);

  // Sphere Intersection.
  __device__ static bool SphereIntersect(const device::Ray& ray,
                                         const device::Sphere& sphere,
                                         device::Intersection& intersection);
protected:
private:
};

} // namespace pathtracer

#endif // _CRT_IINTERSECTIONENGINE_H_