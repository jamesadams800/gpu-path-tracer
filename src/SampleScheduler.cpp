#include "SampleScheduler.h"

#include "Framebuffer.h"

namespace pathtracer {

SampleScheduler::SampleScheduler(ILogger& logger, const Framebuffer& framebuffer)
  : logger_{ logger },
    continuous_rendering_{false},
    number_of_passes_{1},
    super_samples_kernel_size_{1},
    buffer_x_res_{ framebuffer.GetWidth() },
    buffer_y_res_{ framebuffer.GetHeight() },
    max_samples_per_space_{1000000},
    queue_mutex_{},
    sample_queue_{},
    queue_max_size_{ 0 } {
}

bool SampleScheduler::GetSample(SampleSpace& sample) {
  std::unique_lock<std::mutex> lock(queue_mutex_);
  if (!sample_queue_.empty()) {
    sample = sample_queue_.front();
    sample_queue_.pop();
    if (continuous_rendering_) {
      sample_queue_.push(sample);
    }
    logger_.LogReplace("Render Progress " + 
      logger_.FormatPrecision(100.0f - (100.0f * (sample_queue_.size() /
                              static_cast<float>(queue_max_size_)))) + "%");
    return true;
  }
  return false;
}

void SampleScheduler::GenerateSampleSpaces() {
  // For now just schedule rays in a square.
  uint32_t remaining_samples = max_samples_per_space_ / pow(super_samples_kernel_size_, 2);
  uint32_t sample_space_size = sqrt(remaining_samples);
  {
    std::unique_lock<std::mutex> lock(queue_mutex_);
    for (uint32_t x = 0; x <= buffer_x_res_; x += sample_space_size) {
      for (uint32_t y = 0; y <= buffer_y_res_; y += sample_space_size) {
        for (uint32_t i = 0; i < number_of_passes_; i++) {
          SampleSpace sample = { 0 };
          sample.res_x = buffer_x_res_;
          sample.res_y = buffer_y_res_;
          sample.start_x = x;
          sample.start_y = y;
          sample.end_x = std::min(x + sample_space_size, buffer_x_res_);
          sample.end_y = std::min(y + sample_space_size, buffer_y_res_);
          sample.pixel_ss_kernel_size = super_samples_kernel_size_;
          sample_queue_.push(sample);
        }
      }
    }
    queue_max_size_ = sample_queue_.size();
  }
}

std::ostream & operator<<(std::ostream & os, const SampleSpace & sample) {
  os << "[" << sample.start_x << "," << sample.start_y << "] - > ["
     << sample.end_x << "," << sample.end_y << "] SS [" << sample.pixel_ss_kernel_size
     << "x" << sample.pixel_ss_kernel_size << "]";
  return os;
}

} // namespace pathtracer
