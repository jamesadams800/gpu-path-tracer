#ifndef _CRT_RENDERTARGET_H_
#define _CRT_RENDERTARGET_H_

#include <stdint.h>
#include <memory>
#include <mutex>

#include <glm\glm.hpp>

namespace pathtracer {

struct Sample {
  // Sample Normalised screen space coords.
  glm::vec2 ss_cord;
  // RGBA Colour value
  glm::vec3 color;
  float depth;
};

class RenderTarget {
public:
  virtual ~RenderTarget() {}
  virtual void HandleSample(const Sample& sample) = 0;
private:
};

} // namespace pathtracer 

#endif // _CRT_RENDERTARGET_H_