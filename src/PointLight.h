#ifndef CRT_POINTLIGHT_H_
#define CRT_POINTLIGHT_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "Light.h"

namespace pathtracer {

class PointLight : public Light {
public:
  static constexpr float LIGHT_RADIUS = 0.010f;

  explicit PointLight(const glm::vec3& position, const glm::vec3& color, float intensity = 1.0f);
  explicit PointLight(const glm::vec3& color, float intensity = 1.0f);

  glm::vec3 GetPosition() const {
    return position_;
  }

protected:
  glm::vec3 position_;
};

} // namespace pathtracer 

#endif // CRT_POINTLIGHT_H_