#ifndef _CRT_NAIVEINTERSECTIONENGINE_H_
#define _CRT_NAIVEINTERSECTIONENGINE_H_

#include <thrust/device_vector.h>

#include "DeviceRunnable.h"
#include "IntersectionEngine.h"
#include "DeviceGeometry.h"

namespace pathtracer {

class RenderDevice;

class NaiveIntersectionEngine : public DeviceRunnable, public IntersectionEngine {
public:
  NaiveIntersectionEngine(RenderDevice& device);
  void Intersect(const thrust::device_vector<device::Ray>& rays,
                 const DeviceGeometry& geometry,
                 thrust::device_vector<device::Intersection>&) override;

  __device__ static void Intersect(const device::Ray&, 
                                   const device::Geometry&,
                                   device::Intersection&);

  __device__ static bool MeshIntersect(const device::Ray& ray,
                                       const device::Mesh& mesh,
                                       uint32_t& triangle_index,
                                       float& t,
                                       glm::vec3& barycentric);

private:
};

} // namespace pathtracer

#endif // _CRT_NAIVEINTERSECTIONENGINE_H_