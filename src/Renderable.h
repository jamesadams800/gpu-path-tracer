#ifndef CRT_RENDERABLE_H_
#define CRT_RENDERABLE_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "Entity.h"
#include "Material.h"
#include "IDeviceCopyable.h"

namespace pathtracer {

class Renderable : public Entity, public IDeviceCopyable {
public:
  void SetMaterial(const Material* material) {
    material_ = material;
  }

  const Material* GetMaterial() const {
    return material_;
  }
private:
  const Material* material_;
};

} // namespace pathtracer

#endif // CRT_DRAWABLE_H_