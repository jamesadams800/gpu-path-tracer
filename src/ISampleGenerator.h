#ifndef CRT_ISAMPLEGENERATOR_H_
#define CRT_ISAMPLEGENERATOR_H_

#include <thrust/device_vector.h>

namespace pathtracer {

struct Sample;
struct SampleSpace;

class ISampleGenerator {
public:
  virtual ~ISampleGenerator() {}
  // Returns number of samples generated.
  virtual uint32_t GenerateSamples(const SampleSpace&,
                                   thrust::device_vector<Sample>&) = 0;
private:
};

} // namespace pathtracer

#endif // CRT_ISAMPLEGENERATOR_H_