#ifndef _CRT_SCENELOADER_H_
#define _CRT_SCENELOADER_H_

#include <stdexcept>
#include <functional>

#include <assimp\scene.h>
#include <assimp\mesh.h>
#include <assimp\camera.h>

#include "Scene.h"
#include "Mesh.h"
#include "Light.h"
#include "Material.h"
#include "Texture.h"

namespace pathtracer {

class SceneLoadException : public std::runtime_error {
public:
  explicit SceneLoadException(const std::string& err)
    : std::runtime_error(err) {
  }
private:
};

class SceneLoader {
public:
  SceneLoader();
  void LoadScene(const std::string& file_name, Scene& scene);
private:
  uint64_t GetGlobalMaterialIndex(uint32_t local_index);

  static uint64_t file_material_offset_;
  uint32_t material_count_;
  Mesh* BuildMesh(Scene& scene, const aiMesh*, const aiNode*);
  Material* BuildMaterial(Scene& scene, const aiMaterial*);
  glm::mat4 BuildMatrix(const aiMatrix4x4&);
  const aiNode* FindNode(const aiNode* node, const aiString& component_name);

  std::unordered_map< uint64_t, uint64_t > material_global_file_index_to_scene_;
};

} // namespace pathtracer 

#endif // _CRT_SCENELOADER_H_
