#ifndef CRT_UNIFORMSAMPLEGENERATOR_H_
#define CRT_UNIFORMSAMPLEGENERATOR_H_

#pragma warning(push, 0)
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#pragma warning(pop)

#include "DeviceRunnable.h"
#include "ISampleGenerator.h"

namespace pathtracer {

class RenderDevice;

class UniformSampleGenerator : public DeviceRunnable, public ISampleGenerator {
public:
  UniformSampleGenerator(RenderDevice& device);
  uint32_t GenerateSamples(const SampleSpace&, thrust::device_vector<Sample>&) override;

  __device__ static inline glm::vec2 IndexToNormSSCoord(const SampleSpace& input,
                                                        uint32_t kernel_index);

private:
};

} // namespace pathtracer

#endif // CRT_UNIFORMSAMPLEGENERATOR_H_