#ifndef _CRT_COMPLEXCAMERARAYGENERATOR_H_
#define _CRT_COMPLEXCAMERARAYGENERATOR_H_

#include <curand.h>
#include <curand_kernel.h>

#include "IRayGenerator.h"
#include "DeviceRng.h"

namespace pathtracer {

class RealisticCameraRayGenerator : public DeviceRunnable, public IRayGenerator {
public:
  explicit RealisticCameraRayGenerator(RenderDevice& device);

  void GeneratePrimaryRays(const thrust::device_vector<Sample>&,
                           const thrust::device_ptr<device::Camera>,
                           thrust::device_vector<device::Ray>&,
                           thrust::device_vector<float>&) override;

  __device__ static float GenPrimaryRay(device::Camera* camera, 
                                        device::Sampler& sampler,
                                        const glm::vec2& ss_cord,
                                        device::Ray& primary_ray);

private:
};

} // namespace pathtracer

#endif // _CRT_COMPLEXCAMERARAYGENERATOR_H_