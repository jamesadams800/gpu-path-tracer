#ifndef CRT_THREADEVENT_H_
#define CRT_THREADEVENT_H_

#include <thrust/host_vector.h>

#include "RenderTarget.h"
#include "RenderSystem.h"

namespace pathtracer {

class Scene;
class AccumulatorThread;
class SampleScheduler;
class Framebuffer;

struct ThreadEvent {

  enum EventIdentifier {
    THREAD_INIT_REQ,
    THREAD_TERMINATE_REQ,
    ACCUMULATOR_ADD_SAMPLE_REQ,
    ACCUMULATOR_WRITE_FRAMEBUFFER
  };

  struct RenderThreadInit {
    const Scene* scene;
    const RenderSystem::Options* options;
    AccumulatorThread* accumulator;
    SampleScheduler* scheduler;
  };

  struct AccumulatorAddSample {
    std::unique_ptr<thrust::host_vector<Sample>> samples;
  };

  struct AccumulatorWriteFrame {
    Framebuffer* framebuffer;
  };

  explicit ThreadEvent(EventIdentifier id)
    : id{ id } {
    if (id == THREAD_INIT_REQ) {
      render_thread_init.scene = nullptr;
      render_thread_init.accumulator = nullptr;
      render_thread_init.scheduler = nullptr;
    }
    else if (id == ACCUMULATOR_ADD_SAMPLE_REQ) {
      new (&add_sample.samples) std::unique_ptr<thrust::host_vector<Sample>>();
    }
    else if (id == ACCUMULATOR_WRITE_FRAMEBUFFER) {
      write_frame.framebuffer = nullptr;
    }
  }

  ~ThreadEvent() {
    if (id == ACCUMULATOR_ADD_SAMPLE_REQ) {
      // We DO NOT want to delete an add request that still owns a sample.
      // this means something has gone horribly wrong.
      assert(add_sample.samples == nullptr);
      add_sample.samples.~unique_ptr<thrust::host_vector<Sample>>();
    }
  }

  EventIdentifier id;
  union {
    RenderThreadInit render_thread_init;
    AccumulatorAddSample add_sample;
    AccumulatorWriteFrame write_frame;
  };
};

}

#endif // CRT_THREADEVENT_H_
