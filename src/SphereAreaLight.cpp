#include "SphereAreaLight.h"

#include "RenderDevice.h"

namespace pathtracer {

SphereAreaLight::SphereAreaLight(float radius, const glm::vec3 & color, float intensity)
  : radius_{ radius },
    Light(color, intensity) {
}

SphereAreaLight::SphereAreaLight(const glm::vec3 & color, float intensity)
  : SphereAreaLight(1.0f, color, intensity) {
}

} // namespace pathtracer
