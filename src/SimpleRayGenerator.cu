#include "SimpleRayGenerator.h"

#include <cuda_runtime.h>
#include <math_constants.h>

#include "CameraBase.h"
#include "SampleScheduler.h"
#include "RenderTarget.h"
#include "RenderDevice.h"
#include "CudaException.h"
#include "DeviceArray.h"
#include "DeviceLight.h"
#include "DeviceInterp.h"

namespace pathtracer {

__global__ void PrimaryRayGenKernel(const device::Array<const Sample> samples,
                                    device::Camera* camera,
                                    device::Array<device::Ray> rays,
                                    device::Array<float> weights) {
  uint32_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if( kernel_index < rays.size ) {
    weights.data[kernel_index] = 1.0f;
    glm::vec2 ss_coord = samples.data[kernel_index].ss_cord;
    rays.data[kernel_index] = SimpleRayGenerator::GenPrimaryRay(camera, ss_coord);
  }
}

SimpleRayGenerator::SimpleRayGenerator(RenderDevice& device) 
  : DeviceRunnable{ device } {
}

void SimpleRayGenerator::GeneratePrimaryRays(const thrust::device_vector<Sample>& samples,
                                             const thrust::device_ptr<device::Camera> camera,
                                             thrust::device_vector<device::Ray>& primary_rays,
                                             thrust::device_vector<float>& weights) {
  assert(primary_rays.size() != 0 && primary_rays.size() == weights.size());
  auto config = this->GetDevice().GetLaunchConfig(primary_rays.size());
  PrimaryRayGenKernel<<<config.first, config.second>>>(device::ToArray(samples),
                                                       thrust::raw_pointer_cast(camera),
                                                       device::ToArray(primary_rays),
                                                       device::ToArray(weights) );
  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
}

__host__ __device__ 
device::Ray SimpleRayGenerator::GenPrimaryRay(device::Camera* camera,
                                              glm::vec2 screen_space_coord) {
  float angle = tan(CUDART_PI_F * 0.5 * camera->pinhole.fov / 180.0f);
  float ray_dir_x = (2 * screen_space_coord.x - 1) * angle * camera->aspect_ratio;
  float ray_dir_y = (1 - 2 * screen_space_coord.y) * angle;

  glm::vec3 camera_direction = glm::normalize(camera->at - camera->eye);
  glm::vec3 right_vector = glm::normalize(glm::cross(camera_direction, camera->up));
  glm::vec3 up_vector = glm::normalize(glm::cross(right_vector, camera_direction));

  glm::vec3 image_plane = ray_dir_x * right_vector +
                          ray_dir_y * up_vector +
                          camera->eye + camera_direction;

  glm::vec3 ray_direction = glm::normalize(image_plane - camera->eye);

  return device::Ray(camera->eye, ray_direction);
}

} // namespace pathtracer
