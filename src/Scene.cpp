#include "Scene.h"

#include <iostream>
#include <string>

#include <cuda_runtime.h>

#include "CudaException.h"
#include "SceneLoader.h"
#include "SphereAreaLight.h"
#include "Texture.h"
#include "Material.h"
#include "RenderDevice.h"
#include "Sphere.h"
#include "CameraBase.h"

namespace pathtracer {

Scene::Scene() {
}

Scene::~Scene() {
}

void Scene::AddLight(SphereAreaLight * light) {
  std::unique_ptr<Material> light_mat = std::make_unique<Material>();
  auto color = light->GetColor() * light->GetIntensity();
  light_mat->SetEmissive(color.r, color.g , color.b);

  std::unique_ptr<Sphere> light_sphere = std::make_unique<Sphere>(light->GetRadius());
  light_sphere->SetMaterial(light_mat.get());
  light_sphere->SetTransform(light->GetWorldTransform());
  this->AddRenderable(light_sphere.release());
  this->AddMaterial(std::move(light_mat));
}

void Scene::AddCamera(CameraBase * camera) {
  cameras_.push_back(std::unique_ptr<CameraBase>(camera));
}

void Scene::AddRenderable(Renderable * renderable) {
  assert(renderable->GetMaterial());
  renderables_.push_back(std::unique_ptr<Renderable>(renderable));
}

void Scene::AddMaterial(std::unique_ptr<Material> material) {
  material_library_.insert(std::make_pair(material->GetUuid(), std::move(material)));
}

const Material * Scene::GetMaterial(uint32_t id) {
  auto& material = material_library_.find(id);
  if (material != material_library_.end()) {
    return material->second.get();
  }
  return nullptr;
}

void Scene::CopyToDevice(RenderDevice & device) const {
  // Materials
  for (auto& material : material_library_) {
    material.second->CopyToDevice(device);
  }
  // Renderables.
  for (auto& renderable : renderables_) {
    renderable->CopyToDevice(device);
  }
  // Cameras.
  for (auto& camera : cameras_) {
    camera->CopyToDevice(device);
  }
}

} // namespace raytacer
