#ifndef CRT_UTILITIES_H_
#define CRT_UTILITIES_H_

#include <cuda_runtime.h>
#include <math_constants.h>

#pragma warning(push, 0)
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#pragma warning(pop)

namespace pathtracer {

class Utilities {
public:
  __device__ __host__
  inline static glm::mat4 CoordinateSystem(const glm::vec3& normal);

  __device__ __host__
  inline static glm::mat4 CoordinateSystem(const glm::vec3& at, 
                                           const glm::vec3& right,
                                           const glm::vec3& up);

  __device__ __host__
  inline static glm::mat4 NormalMatrix(glm::mat4& transform) {
    return glm::transpose(glm::inverse(transform));
  }

};

__device__ __host__
glm::mat4 Utilities::CoordinateSystem(const glm::vec3& tangent,
                                      const glm::vec3& normal,
                                      const glm::vec3& bitangent) {
  glm::mat4 mat{1};

  mat[0][0] = tangent.x;
  mat[0][1] = tangent.y;
  mat[0][2] = tangent.z;
  mat[1][0] = normal.x;
  mat[1][1] = normal.y;
  mat[1][2] = normal.z;
  mat[2][0] = bitangent.x;
  mat[2][1] = bitangent.y;
  mat[2][2] = bitangent.z;

  return mat;
}

glm::mat4 Utilities::CoordinateSystem(const glm::vec3& normal) {
  glm::mat4 mat = {};
  glm::vec3 tangent = {};

  if (fabs(normal.x) > fabs(normal.y)) {
    tangent.x = normal.z;
    tangent.y = 0.0f;
    tangent.z = -normal.x;
  }
  else {
    tangent.x = 0.0f;
    tangent.y = -normal.z;
    tangent.z = normal.y;
  }
  tangent = glm::normalize(tangent);
  glm::vec3 bitangent = glm::cross(normal, tangent);

  return Utilities::CoordinateSystem(tangent, normal, bitangent);
}

} // namespace pathtracer

#endif // CRT_UTILITIES_H_