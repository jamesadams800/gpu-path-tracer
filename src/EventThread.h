#ifndef CRT_SYSTEMTHREAD_H_
#define CRT_SYSTEMTHREAD_H_

#include <stdint.h>
#include <vector>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <queue>

#include "ThreadEvent.h"
#include "ILogger.h"

namespace pathtracer {

class EventThread {
public:
  explicit EventThread(ILogger& logger);
  virtual ~EventThread();
  EventThread(const EventThread&) = delete;
  EventThread& operator=(EventThread&) = delete;

  uint32_t GetId() const {
    return thread_index_;
  }

  bool IsWaiting() const {
    return waiting_.load();
  }

  bool HasUnhandledEvents() const {
    std::unique_lock<std::mutex> lock(mutex_);
    return events_.size();
  }

  void StartThread();
  void StopThread();

protected:
  virtual void ThreadMain() = 0;
  void SendEvent(std::unique_ptr<ThreadEvent>);
  std::unique_ptr<ThreadEvent> GetNextEvent(bool blocking = true);

  ILogger& Logger() const {
    return logger_;
  }

private:
  uint32_t thread_index_;
  std::atomic<bool> waiting_;
  std::unique_ptr<std::thread> thread_;
  std::queue< std::unique_ptr<ThreadEvent> > events_;
  mutable std::mutex mutex_;
  std::condition_variable condition_;
  ILogger& logger_;

  static uint32_t thread_index_counter_;
};

} // namespace pathtracer

#endif // CRT_SYSTEMTHREAD_H_