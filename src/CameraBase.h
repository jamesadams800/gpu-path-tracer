#ifndef CRT_CAMERABASE_H_
#define CRT_CAMERABASE_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "Entity.h"
#include "IDeviceCopyable.h"

namespace pathtracer {

class CameraBase : public Entity, public IDeviceCopyable {
public:
  explicit CameraBase(float aspect_ratio)
    : aspect_ratio_{ aspect_ratio },
      up_{0.0f, 1.0f, 0.0f},
      at_{ 0.0f, 0.0f, -1.0f },
      eye_{ 0.0f, 0.0f, 0.0f } {
  }
 
  virtual void LookAt(const glm::vec3& at) {
    at_ = at;
  }

  void LookAt(float x, float y, float z) {
    this->LookAt(glm::vec3{ x,y,z });
  }

  glm::vec3 GetAt() const {
    return at_;
  }

  glm::vec3 GetEye() const {
    return eye_;
  }

  glm::vec3 GetUp() const {
    return up_;
  }

  float GetAspectRatio() const {
    return aspect_ratio_;
  }


protected:
private:
  glm::vec3 up_;
  glm::vec3 eye_;
  glm::vec3 at_;
  float aspect_ratio_;
};

} // namespace pathtracer

#endif // CRT_CAMERABASE_H_