#ifndef CRT_RENDERTHREAD_H_
#define CRT_RENDERTHREAD_H_

#include "EventThread.h"

#include "RenderDevice.h"
#include "RenderSystem.h"
#include "Integrator.h"

namespace pathtracer {

class AccumulatorThread;
class SampleScheduler;

class RenderThread : public EventThread {
public:
  explicit RenderThread(RenderDevice&, ILogger&, uint32_t device_index);
  RenderThread(const RenderThread&) = delete;
  RenderThread& operator=(RenderThread&) = delete;

  void SendInit(const Scene*,
                const RenderSystem::Options*,
                AccumulatorThread*,
                SampleScheduler*);

private:
  void ThreadMain() override;
  void MainLoop();

  void CheckThreadEvents(bool blocking);

  void ThreadInitialise(const Scene*,
                        const RenderSystem::Options* config,
                        AccumulatorThread*,
                        SampleScheduler*);

  void ThreadTerminate();

  bool exit_;
  bool initialised_;
  // Accumulator thread pointer used to send completed sample vectors into accumulation buffer.
  AccumulatorThread* accumulator_thread_;
  // Scheduler object pointer used to grab next SampleSpace for rendering on this thread.
  SampleScheduler* scheduler_;
  RenderDevice& render_device_;
  std::unique_ptr<Integrator> integrator_;
};

} // namespace pathtracer 

#endif // 