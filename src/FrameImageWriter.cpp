#include "FrameImageWriter.h"

#include <vector>
#include <algorithm>
#include <iostream>
#include <memory>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

template <typename T>
T FrameImageWriter::GetNormalisedInteger(float channel_value) {
  float clamped = std::max(0.0f, std::min(1.0f, channel_value));
  return static_cast<T>(clamped * static_cast<float>(std::numeric_limits<T>::max()));
}

FrameImageWriter::FrameImageWriter() {
}

FrameImageWriter::~FrameImageWriter() {
}

void FrameImageWriter::Write(const std::string& file_name,
                             const pathtracer::Framebuffer & framebuffer,
                             Channel channel) {
  if (channel == COLOR) {
    this->WriteColor(file_name, framebuffer);
  }
  else if (channel == DEPTH) {
    this->WriteDepth(file_name, framebuffer);

  }
}

void FrameImageWriter::WriteColor(const std::string& file_name,
                                  const pathtracer::Framebuffer& framebuffer) {
  std::vector< uint8_t > pixels;
  pixels.reserve(3 * framebuffer.GetWidth() * framebuffer.GetHeight());
  for (uint32_t i = 0; i < framebuffer.GetHeight(); i++) {
    for (uint32_t j = 0; j < framebuffer.GetWidth(); j++) {
      pathtracer::Framebuffer::Pixel pixel = framebuffer.Get(j, i);
      pixels.push_back(GetNormalisedInteger<uint8_t>(pixel.color.r));
      pixels.push_back(GetNormalisedInteger<uint8_t>(pixel.color.g));
      pixels.push_back(GetNormalisedInteger<uint8_t>(pixel.color.b));
    }
  }

  stbi_write_png((file_name + ".png").c_str(),
                 framebuffer.GetWidth(),
                 framebuffer.GetHeight(),
                 3,
                 pixels.data(),
                 0);
}

void FrameImageWriter::WriteDepth(const std::string& file_name, const pathtracer::Framebuffer& framebuffer) {
  std::vector<uint8_t> depths;
  depths.reserve(framebuffer.GetWidth() * framebuffer.GetHeight());
  for (uint32_t i = 0; i < framebuffer.GetHeight(); i++) {
    for (uint32_t j = 0; j < framebuffer.GetWidth(); j++) {
      pathtracer::Framebuffer::Pixel pixel = framebuffer.Get(j, i);
      depths.push_back(GetNormalisedInteger<uint8_t>(pixel.depth));
    }
  }

  stbi_write_png((file_name + ".png" ).c_str(),
                 framebuffer.GetWidth(),
                 framebuffer.GetHeight(),
                 1,
                 depths.data(),
                 0);
}
