#ifndef _CRT_RENDERPIPELINE_H_
#define _CRT_RENDERPIPELINE_H_

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include "Ray.h"
#include "DeviceBrdf.h"
#include "DeviceRng.h"
#include "DeviceGeometry.h"
#include "Intersection.h"
#include "NaiveIntersectionEngine.h"

namespace pathtracer {

class RenderDevice;
class ISampleGenerator;
class IRayGenerator;
class ShadingEngine;
struct SampleSpace;
struct Sample;

class Integrator {
public:
  explicit Integrator(RenderDevice& device);
  virtual ~Integrator();

  virtual std::unique_ptr<thrust::host_vector<Sample>> Render(const SampleSpace& input) = 0;

  __device__ static inline glm::vec3 DirectIllumination(device::Sampler& sampler,
                                                        const device::Geometry& geometry,
                                                        const device::Geometry& emmissive_geometry,
                                                        const device::Ray& wo,
                                                        const device::Intersection& intersection);

  __device__ static inline glm::vec3 SampleRandomLight(device::Sampler& sampler,
                                                       const device::Geometry& emitters);

  void SetLightBounces(uint32_t bounces) {
    light_bounces_ = bounces;
  }

protected:
  RenderDevice& device_;
  uint32_t light_bounces_;
};

__device__ glm::vec3 Integrator::DirectIllumination(device::Sampler & sampler,
                                                    const device::Geometry& geometry,
                                                    const device::Geometry& emissive_geometry,
                                                    const device::Ray & incoming_ray,
                                                    const device::Intersection & intersection) {
  glm::vec3 light_point = Integrator::SampleRandomLight(sampler, emissive_geometry);
  glm::vec3 light_direction = light_point - intersection.position;
  device::Ray shadow_ray(intersection.position + (intersection.normal * 0.001f), glm::normalize(light_direction));
  device::Intersection shadow_intersection;
  NaiveIntersectionEngine::Intersect(shadow_ray,
                                     geometry,
                                     shadow_intersection);
  if (shadow_intersection.has_hit) {
    glm::vec3 emitted = shadow_intersection.brdf->Le(shadow_intersection);
    glm::vec3 brdf = intersection.brdf->Evaluate(intersection,
                                                 -incoming_ray.GetDirection(),
                                                 shadow_ray.GetDirection());
    return emitted * brdf;
  }
  return glm::vec3(0.0f);
}

__device__ glm::vec3 Integrator::SampleRandomLight(device::Sampler& sampler,
                                                     const device::Geometry& emitters) {
  uint32_t total_emitters = emitters.meshes.size + emitters.spheres.size;
  uint32_t random_index = sampler.SampleUint() % total_emitters;

  if (random_index < emitters.spheres.size) {
    glm::vec3 sphere_sample = sampler.SampleSphereSurface(emitters.spheres.data[random_index].radius);
    return sphere_sample + emitters.spheres.data[random_index].position;
  }
  else {
    uint32_t mesh_index = random_index - emitters.spheres.size;
    const device::Mesh& mesh = emitters.meshes.data[mesh_index];
    uint32_t random_vertex = sampler.SampleUint() % mesh.vertex_count;
    glm::vec2 rand = sampler.UniformSample<glm::vec2>();
    float sqrt_r1 = sqrtf(rand.x);
    glm::vec3 barycentric(1 - sqrt_r1,
                          sqrt_r1 * (1 - rand.y),
                          sqrt_r1 * rand.y);
    return device::Interp::BarycentricAttrib<glm::vec3>(mesh, 
                                                        VertexBuffer::POSITION,
                                                        random_vertex,
                                                        barycentric);
  }
}


}

#endif //_CRT_RENDERPIPELINE_H_