#ifndef _CRT_APPLICATION_H_
#define _CRT_APPLICATION_H_

#include "ILogger.h"
#include "ArgumentParser.h"

namespace pathtracer {

class Scene;
class RenderSystem;

class Application {
public:
  Application(int argc, char** argv);
  ~Application();
  int32_t Run();
private:
  void InitScene();
  void RenderFrame();

  ArgumentParser arg_parser_;
  std::unique_ptr<ILogger> logger_;
  std::unique_ptr<Scene> scene_;
  std::unique_ptr<RenderSystem> renderer_;
};

} // namespace pathtracer

#endif // _CRT_APPLICATION_H_