#ifndef CRT_ENTITY_H_
#define CRT_ENTITY_H_

#include <stdint.h>
#include <vector>
#include <memory>

#pragma warning(push, 0)
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#pragma warning(pop)

namespace pathtracer {

class Entity {
public:
  Entity();
  virtual ~Entity() = default;

  glm::mat4 GetWorldTransform() const {
    return transform_matrix_;
  }

  void SetTranslation(const glm::vec3& vec);
  void SetTranslation(float x, float y, float z);
  void SetTransform(const glm::mat4& mat);
  void SetRotation(const glm::vec3& axis, float rad);

protected:
  glm::mat4 transform_matrix_;
};

} // namespace pathtracer

#endif // CRT_ENTITY_H_