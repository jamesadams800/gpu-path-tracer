#include "Application.h"

#if _WIN32
#include <Windows.h>
#endif
#include <iostream>

#include "DefaultLogger.h"
#include "RenderSystem.h"
#include "Framebuffer.h"
#include "FrameImageWriter.h"
#include "PointLight.h"
#include "SphereAreaLight.h"
#include "PinholeCamera.h"
#include "RealisticCamera.h"
#include "SceneLoader.h"

namespace pathtracer {

Application::Application(int argc, char ** argv)
  : arg_parser_{ argc, argv },
    logger_{ new DefaultLogger() },
    scene_{ new Scene() },
    renderer_{ new RenderSystem(*logger_) } {
}

Application::~Application() {
}

int32_t Application::Run() {
  try {
    this->InitScene();
    this->RenderFrame();
  }
  catch (std::runtime_error& ex) {
#if _WIN32
    MessageBox(nullptr, ex.what(), "pathtracer Error", 0);
#else
    std::cout << "ERROR: pathtracer Exception: " << ex.what() << std::endl;
#endif//
    return -1;
  }
  logger_->Log("Press enter to exit...");
  std::cin.get();

  return 0;
}

void Application::InitScene() {
  pathtracer::SceneLoader loader;

  logger_->Log("Initialising scene.");
  if (arg_parser_.IsPresent("-l")) {
    auto load_list = arg_parser_.GetArgList("-l");
    for (auto& path : load_list) { 
      logger_->Log("Loading file " + path);
      loader.LoadScene(path, *scene_);
    }
  }
  else {
    loader.LoadScene("..\\assets\\models\\CornellBox-Scaled.obj", *scene_);
  }

  uint32_t x_res;
  uint32_t y_res;
  if (arg_parser_.Get("-w", x_res) && arg_parser_.Get("-h", y_res)) {
  }
  else {
    x_res = 1920;
    y_res = 1080;
  }

  // Scene Description.
  RealisticCamera* real_camera = new pathtracer::RealisticCamera(static_cast<float>(x_res) / 
                                                                 static_cast<float>(y_res));
  //real_camera->SetTranslation(0.0f, 0.0f,3500.0f);
  //real_camera->LookAt(5.0f, 1980.0f, -30.0f);
  //LensSystemFactory::AddDoubleGauss(*real_camera);

  //PinholeCamera* real_camera = new PinholeCamera(90.0f);

  //1.
  //real_camera->SetTranslation(0.0f, 810.0f, 1400.0f);
  //real_camera->LookAt(-5.0f, 1980.0f, -30.0f);
  //real_camera->LookAt(0.0f, 1200.0f, 500.0f);
  //2.
  //real_camera->SetTranslation(0.0f, 810.0f, 700.0f);
  //real_camera->LookAt(0.0f,1400.0f,-100.f);
  //pathtracer::LensSystemFactory::AddFishEye(*real_camera);

  //1.
  real_camera->SetTranslation(-200.0f, 1400.f, 3200.0f);
  real_camera->LookAt(0.0f, 1000.0f, 100.0f);
  pathtracer::LensSystemFactory::AddWideAngle(*real_camera);

  //2.
  //real_camera->SetTranslation(200.0f, 700.f, 3400.0f);
  //real_camera->LookAt(0.0f, 1000.0f, 100.0f);
  //real_camera->LookAt(-800.0, 400.0f, 570.0);
  //pathtracer::LensSystemFactory::AddWideAngle(*real_camera);
  
  //real_camera->SetTranslation(0.0f, 1500.0f, 40000.0f);
  //real_camera->LookAt(-50.0, 600.0, 570.0);
  //pathtracer::LensSystemFactory::AddTelephoto(*real_camera);


  scene_->AddCamera(real_camera);
  /*SphereAreaLight* sphere_light = new SphereAreaLight(5.0f, { 0.87f, 0.9f, 0.83f }, 0.4f);
  sphere_light->SetTranslation(-240.0f, 1970.0f, 160.0f);
  scene_->AddLight(sphere_light);

  SphereAreaLight* sphere_light2 = new SphereAreaLight(5.0f, { 0.87f, 0.9f, 0.83f }, 0.4f);
  sphere_light2->SetTranslation(-240.0f, 1970.0f, -220.f);
  scene_->AddLight(sphere_light2);

  SphereAreaLight* sphere_light3 = new SphereAreaLight(5.0f, { 0.87f, 0.9f, 0.83f }, 0.4f);
  sphere_light3->SetTranslation(230.0f, 1970.0f, -220.0f);
  scene_->AddLight(sphere_light3);

  SphereAreaLight* sphere_light4 = new SphereAreaLight(5.0f, { 0.87f, 0.9f, 0.83f }, 0.4f);
  sphere_light4->SetTranslation(230.0f, 1970.0f, 160.0f);
  scene_->AddLight(sphere_light4);*/

  //SphereAreaLight* sphere_light = new SphereAreaLight(2.0f, { 0.87f, 0.9f, 0.83f }, 0.8f);
  //sphere_light->SetTranslation(0.0f, 1200.0f, 100.0f);
  //scene_->AddLight(sphere_light);
  
  //SphereAreaLight* sphere_light2 = new SphereAreaLight(50.0f, { 0.87f, 0.12f, 0.5f }, 5.8f);
  //sphere_light2->SetTranslation(0.0f, 1200.0f, 500.0f);
  //scene_->AddLight(sphere_light2);
  // Set Renderer Options.

  RenderSystem::Options options;
#if 0
  options.ss_per_pixel = 10;
  options.passes = 3;
#elif 1
  options.light_bounces = 5;
  options.ss_per_pixel = 5;
  options.passes = 168;
#else
  options.ss_per_pixel = 9;
  options.passes = 20;
#endif
  renderer_->Configure(options);
}

void Application::RenderFrame() {
  std::string file_prefix;
  uint32_t x_res;
  uint32_t y_res;

  if (!arg_parser_.Get("-o", file_prefix)) {
    // If no arg present.
    file_prefix = "OutputRender";
  }
  if (arg_parser_.Get("-w", x_res) && arg_parser_.Get("-h", y_res)) {
  }
  else {
    x_res = 640;
    y_res = 480;
  }

  logger_->Log("Starting Render System.");
  pathtracer::Framebuffer framebuffer(x_res, y_res);
  renderer_->Render(*scene_, framebuffer);

  FrameImageWriter writer;
  logger_->Log("Writing result to " + file_prefix + ".png");
  writer.Write(file_prefix, framebuffer, FrameImageWriter::COLOR);
}

};