#include "DeviceCameraManager.h"

#include <iostream>
#include <stdio.h>

#include <math_constants.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>

#include "RenderDevice.h"
#include "DeviceArray.h"
#include "Ray.h"
#include "DeviceCameraUtilities.h"
#include "CudaException.h"
#include "DevicePrimitives.h"

namespace pathtracer {

__global__ void ExitBoundsKernel(device::Camera* camera) {
  device::RealisticCamera& realistic_camera = camera->realistic;
  uint32_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if (kernel_index < device::RealisticCamera::EXIT_PUPIL_SAMPLES) {
    glm::vec2 film_points = { 
      static_cast<float>(kernel_index) /
      device::RealisticCamera::EXIT_PUPIL_SAMPLES * realistic_camera.film_diag / 2.0f,
      static_cast<float>(kernel_index + 1) /
      device::RealisticCamera::EXIT_PUPIL_SAMPLES * realistic_camera.film_diag / 2.0f };
    realistic_camera.pupil_bounds[kernel_index] = 
      RealisticCameraUtilities::FindExitPupilBounds(realistic_camera,
                                                    film_points);
  }
}

DeviceCameraManager::DeviceCameraManager(RenderDevice & device)
  : device_{ device },
    camera_type_{device::Camera::Type::PINHOLE},
    camera_{ thrust::device_malloc<device::Camera>(1) } {
}

DeviceCameraManager::~DeviceCameraManager() {
  thrust::device_free(camera_);
}

void DeviceCameraManager::AddCamera(const PinholeCamera& camera) {
  device::Camera device_camera = {};
  device_camera.aspect_ratio = camera.GetAspectRatio();
  device_camera.pinhole.fov = camera.GetFov();
  device_camera.pinhole.near_plane = camera.GetNearPlane();
  device_camera.eye = camera.GetWorldTransform() * glm::vec4{ camera.GetEye(), 1.0f };
  device_camera.at = camera.GetAt();
  device_camera.up = camera.GetUp();
  device_camera.camera_to_world = glm::inverse(glm::lookAt(device_camera.eye,
                                                           device_camera.at,
                                                           device_camera.up));

  device_camera.type = device::Camera::Type::PINHOLE;
  camera_[0] = device_camera; 
  camera_type_ = device::Camera::Type::PINHOLE;
}

void DeviceCameraManager::AddCamera(const RealisticCamera& camera) {
  device::Camera device_camera = {};

  // Calculate and copy film parameters
  device_camera.realistic.film_diag = camera.GetFilmSize();
  device_camera.aspect_ratio = camera.GetAspectRatio();
  float dimension = sqrtf((camera.GetAspectRatio() *  camera.GetAspectRatio()) + 1.0f);
  float ratio = device_camera.realistic.film_diag / dimension;

  device_camera.realistic.film_y = ratio;
  device_camera.realistic.film_x = ratio * camera.GetAspectRatio();

  // Copy lens system.
  if (camera.GetLenses().size() <= 0) {
    throw std::runtime_error("DeviceCameraManager::AddCamera(const RealisticCamera&) : Unable to"
                             " add realistic camera to device.  No Lens system has been provided.");
  }
  float cummulative_distance = 0.0f;
  int32_t lens_count = 0;
  for (auto& lens : camera.GetLenses()) {
    assert(lens_count < device::RealisticCamera::MAX_LENS_NO);
    device_camera.realistic.lens_system[lens_count].is_stop = lens.is_stop;
    device_camera.realistic.lens_system[lens_count].radius = lens.radius;
    device_camera.realistic.lens_system[lens_count].thickness = lens.thickness;
    device_camera.realistic.lens_system[lens_count].ri = lens.ri;
    device_camera.realistic.lens_system[lens_count].aperture = lens.aperture * 0.5f;;
    cummulative_distance -= lens.thickness;
    lens_count++;
  }
  // Initial values for lens front z and lens rear z.
  // These will be modified again once the focal distance has been calculated.
  device_camera.realistic.lens_front_z = cummulative_distance;
  device_camera.realistic.lens_back_z = device_camera.realistic.lens_system[lens_count - 1].thickness;
  device_camera.realistic.lens_count = lens_count;

  // Find focus.
  float focus = this->FindFocus(device_camera.realistic, camera.GetFocusDistance());
  device_camera.realistic.lens_system[lens_count - 1].thickness -= focus;
  device_camera.realistic.lens_front_z = cummulative_distance + focus;
  device_camera.realistic.lens_back_z = -device_camera.realistic.lens_system[lens_count - 1].thickness;

  // Calculate camera orientation and transform matrices.
  glm::vec3 camera_offset = camera.GetWorldTransform() * glm::vec4{ camera.GetEye(), 1.0f };
  device_camera.eye = camera_offset;
  device_camera.at = camera.GetAt();
  device_camera.up = camera.GetUp();

  device_camera.world_to_camera = glm::lookAt(device_camera.eye, device_camera.at, device_camera.up);
  device_camera.camera_to_world = glm::inverse(device_camera.world_to_camera);

  device_camera.type = device::Camera::Type::REALISTIC;
  camera_[0] = device_camera;
  camera_type_ = device::Camera::Type::REALISTIC;
  // Calculate exit pupil.
  this->CalculateExitPupil();
}

// The find focus and exit pupil searching are also closely adapted from the implementations
// that were written for the PBRT project found at: https://www.pbrt.org/
float DeviceCameraManager::FindFocus(const device::RealisticCamera& camera, 
                                     float focus_distance) const {
  glm::vec2 pz;
  glm::vec2 fz;
  this->CalculateThickLensApprox(camera, pz, fz);

  float f = fz.x - pz.x;
  float z = -focus_distance;
  float c = (pz.y - z - pz.x) * (pz.y - z - 4 * f - pz.x);
  if (c < 0) {
    std::cerr << "Coefficient must be positive." << std::endl;
  }
  float delta = 0.5f * (pz.y - z + pz.x - std::sqrt(c));
  return camera.lens_system[camera.lens_count - 1].thickness + delta;
}

void DeviceCameraManager::CalculateThickLensApprox(const device::RealisticCamera& camera,
                                                   glm::vec2 & pz, 
                                                   glm::vec2 & fz) const {
  float x = 0.001f * camera.film_diag;
  // Compute cardinal points for film side of lens system
  device::Ray scene_ray(glm::vec3(x, 0, camera.lens_front_z - 1000.0f),
                        glm::vec3(0, 0, 1));
  device::Ray film_ray = scene_ray;
  if (!RealisticCameraUtilities::TraceLensSystemFromScene(camera, film_ray)) {
    throw std::runtime_error("Unable to trace ray from scene to film for thick lens approximation.");
  }
  glm::vec2 c_points = RealisticCameraUtilities::CalculateCardinalPoints(scene_ray, film_ray);
  pz.x = c_points.x;
  fz.x = c_points.y;
  
  // Compute cardinal points for scene side of lens system
  film_ray = scene_ray = device::Ray(glm::vec3(x, 0, camera.lens_back_z + 1000.0f),
                                     glm::vec3(0, 0, -1));
  if (!RealisticCameraUtilities::TraceLensSystemFromFilm(camera, scene_ray)) {
    throw std::runtime_error("Unable to trace ray from film to scene for thick lens approximation.");
  }
  c_points = RealisticCameraUtilities::CalculateCardinalPoints(film_ray, scene_ray);
  pz.y = c_points.x;
  fz.y = c_points.y;
}

void DeviceCameraManager::CalculateExitPupil() {
  auto config = device_.GetLaunchConfig(device::RealisticCamera::EXIT_PUPIL_SAMPLES);
  ExitBoundsKernel << <config.first, config.second >> > (thrust::raw_pointer_cast(camera_));
  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
}

} // namespace pathtracer 
