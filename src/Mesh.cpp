#include "Mesh.h"

#include <float.h>

#include "RenderDevice.h"
#include "DeviceScene.h"

namespace pathtracer {

Mesh::Mesh(std::unique_ptr<const VertexBuffer> vertex_buffer,
           std::unique_ptr<const IndexBuffer> index_buffer)
  : vertex_buffer_{ std::move(vertex_buffer) },
    index_buffer_{ std::move(index_buffer) } {
}

void Mesh::CopyToDevice(RenderDevice& device) const {
  device.GetScene().GetGeometry().AddMesh(*this);
}

} // namespace pathtracer
