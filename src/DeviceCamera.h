#ifndef CRT_DEVICECAMERA_H_
#define CRT_DEVICECAMERA_H_

#include <glm/glm.hpp>

#include "DeviceArray.h"
#include "DevicePrimitives.h"

namespace pathtracer {

namespace device {

struct Lens {
  bool is_stop;
  float radius;
  float thickness;
  float ri;
  float aperture;
};

struct PinholeCamera {
  float fov;
  float near_plane;
};

struct Bounds {
  glm::vec2 min;
  glm::vec2 max;
};

struct RealisticCamera {
  // Film data. 35mm film by default.
  float film_diag = 35.0f;
  float film_x;
  float film_y;
  // Lens data.
  static constexpr size_t MAX_LENS_NO = 15;
  float lens_front_z;
  float lens_back_z;
  Lens lens_system[MAX_LENS_NO];
  uint32_t lens_count;
  // Exit pupil.
  static constexpr size_t EXIT_PUPIL_SAMPLES = 64;
  device::Bounds pupil_bounds[EXIT_PUPIL_SAMPLES];
};

struct Camera {
  enum Type {
    PINHOLE,
    REALISTIC
  };

  Camera() = default;
  glm::vec3 up;
  glm::vec3 eye;
  glm::vec3 at;
  glm::mat4 camera_to_world;
  glm::mat4 world_to_camera;

  float aspect_ratio;

  Type type;
  PinholeCamera pinhole;
  RealisticCamera realistic;
};

} // namespace device 

} // namespace pathtracer

#endif // CRT_DEVICECAMERA_H_