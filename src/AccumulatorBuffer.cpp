#include "AccumulatorBuffer.h"

#include <algorithm>

#include "Framebuffer.h"

namespace pathtracer {

AccumulatorBuffer::AccumulatorBuffer(uint32_t width, uint32_t height) 
  : x_res_{ width },
    y_res_{ height },
    data_{ new Pixel[width * height] } {
}

void AccumulatorBuffer::HandleSample(const Sample & sample) {
  uint32_t x = floor(sample.ss_cord.x * (x_res_ ));
  uint32_t y = floor(sample.ss_cord.y * (y_res_ ));
  uint32_t index = (y * x_res_) + x;
  assert(index < (x_res_ * y_res_));
  data_[index].color += sample.color;
  data_[index].depth = std::min(sample.depth, data_[index].depth);
  data_[index].samples++;
}

void AccumulatorBuffer::WriteFramebuffer(Framebuffer & framebuffer) {
  for (uint32_t y = 0; y < y_res_; y++) {
    for (uint32_t x = 0; x < x_res_; x++) {
      uint32_t index = (y * x_res_) + x;
      framebuffer.Set(x, y, this->GeneratePixel(data_[index]));
    }
  }
}

Framebuffer::Pixel AccumulatorBuffer::GeneratePixel(const AccumulatorBuffer::Pixel & accumulator_pixel) {
  Framebuffer::Pixel output_pixel;
  if (accumulator_pixel.samples > 0) {
    glm::vec3 tonemapped = this->ReinhardToneMap(accumulator_pixel.color /
                                                 static_cast<float>(accumulator_pixel.samples));
    output_pixel.color = this->GammaCorrect(tonemapped);
    output_pixel.depth = accumulator_pixel.depth;
  }
  else {
    output_pixel.color = { 0.0f,0.0f,0.0f };
    output_pixel.depth = std::numeric_limits<float>::max();
  }

  return output_pixel;
}

glm::vec3 AccumulatorBuffer::ReinhardToneMap(const glm::vec3 & in_color) {
  if (glm::dot(in_color, in_color) == 0.0f) {
    return glm::vec3(0.0f);
  }
  static glm::vec3 rgb_to_luminance(0.2126f, 0.7152f, 0.0722f);
  static const float white = 8.2f;
  float luminance = glm::dot(rgb_to_luminance, in_color);

  float mapped_luminance = (luminance * (1 + luminance / (white * white)) / (1 + luminance));
  float scale = mapped_luminance / luminance;

  return scale * in_color;
}

glm::vec3 AccumulatorBuffer::GammaCorrect(const glm::vec3 & in_color) {
  auto linear_to_srgb_low = [](float x) -> float {
    return 12.92f * x;
  };
  auto linear_to_srgb_high = [](float x) -> float {
    return 1.055 * powf(x, (1.0f / 2.4f)) - 0.055f; 
  };

  glm::vec3 out(0.0f);

  for (uint32_t i = 0; i < glm::vec3::length(); i++) {
    if (in_color[i] <= 0.00031308) {
      out[i] = linear_to_srgb_low(in_color[i]);
    }
    else {
      out[i] = linear_to_srgb_high(in_color[i]);
    }
  }
  
  return out;
}


} // namespace pathtracer 
