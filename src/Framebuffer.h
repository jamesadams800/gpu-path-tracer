#ifndef _CRT_FRAMEBUFFER_H_
#define _CRT_FRAMEBUFFER_H_

#include <stdint.h>
#include <memory>

#include <glm\glm.hpp>

namespace pathtracer {

class Framebuffer {
public:
  struct Pixel {
    glm::vec3 color;
    float depth;
  };

  explicit Framebuffer(uint32_t width, uint32_t height);

  Framebuffer(const Framebuffer&) = delete;
  Framebuffer& operator=(const Framebuffer&) = delete;

  uint32_t GetWidth() const {
    return x_res_;
  }

  uint32_t GetHeight() const {
    return y_res_;
  }

  // Add framebuffers.
  void operator+=(const Framebuffer& fb2);

  Pixel Get(uint32_t x, uint32_t y) const;
  void Set(uint32_t x, uint32_t y, const Pixel& value);

private:
  uint32_t x_res_;
  uint32_t y_res_;
  std::unique_ptr<Pixel[]> data_;
};

} // namespace pathtracer 

#endif // _CRT_FRAMEBUFFER_H_
