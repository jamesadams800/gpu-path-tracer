#include "NaiveIntersectionEngine.h"

#include <thrust/sort.h>

#include "RenderDevice.h"
#include "CudaException.h"
#include "Timer.h"
#include "DeviceInterp.h"

namespace pathtracer {

__global__ void IntersectionKernel(const device::Array<const device::Ray> rays,
                                   const device::Geometry* descriptor,
                                   device::Array<device::Intersection> intersections) {
  size_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if (kernel_index < rays.size) {
    NaiveIntersectionEngine::Intersect(rays.data[kernel_index],
                                       *descriptor,
                                       intersections.data[kernel_index]);
  }
}

NaiveIntersectionEngine::NaiveIntersectionEngine(RenderDevice & device)
  : DeviceRunnable{ device } {

}

void NaiveIntersectionEngine::Intersect(const thrust::device_vector<device::Ray>& rays,
                                        const DeviceGeometry& geometry,
                                        thrust::device_vector<device::Intersection>& intersections) {
  assert(rays.size() == intersections.size());
  auto config = this->GetDevice().GetLaunchConfig(rays.size());
  const auto descriptor = geometry.GetGeometry();
  IntersectionKernel<<<config.first, config.second>>> (device::ToArray(rays),
                                                       thrust::raw_pointer_cast(descriptor), 
                                                       device::ToArray(intersections));
  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
}

__device__
void NaiveIntersectionEngine::Intersect(const device::Ray& ray,
                                        const device::Geometry& descriptor,
                                        device::Intersection& intersection) {
  for (uint32_t i = 0; i < descriptor.spheres.size; i++) {
    IntersectionEngine::SphereIntersect(ray, 
                                        descriptor.spheres.data[i],
                                        intersection);
  }

  bool mesh_intersection = false;
  uint32_t mesh_intersect_index = 0;
  uint32_t triangle_intersect_index = 0;
  glm::vec3 mesh_barycentric;
  for (uint32_t i = 0; i < descriptor.meshes.size; i++) {
    if (NaiveIntersectionEngine::MeshIntersect(ray,
                                               descriptor.meshes.data[i],
                                               triangle_intersect_index,
                                               intersection.t, mesh_barycentric)) {
      mesh_intersect_index = i;
      mesh_intersection = true;
    }
  }
  
  if (mesh_intersection) {
    auto& mesh = descriptor.meshes.data[mesh_intersect_index];
    intersection.has_hit = true;
    intersection.brdf = mesh.brdf;
    intersection.position = ray.GetOrigin() + ray.GetDirection() * intersection.t;
    intersection.normal = glm::normalize(device::Interp::BarycentricAttrib<glm::vec3>(mesh, VertexBuffer::NORMAL,
                                                                                      triangle_intersect_index,
                                                                                      mesh_barycentric));
    intersection.uv = device::Interp::BarycentricAttrib<glm::vec2>(mesh, VertexBuffer::TEXCOORD0,
                                                                   triangle_intersect_index,
                                                                   mesh_barycentric);
  }
}

__device__ 
bool NaiveIntersectionEngine::MeshIntersect(const device::Ray& ray,
                                            const device::Mesh& mesh,
                                            uint32_t& triangle_index,
                                            float& mesh_t,
                                            glm::vec3& mesh_barycentric) {
  const device::Array<float>& position = mesh.vertex_data[VertexBuffer::POSITION];
  volatile uint32_t triangle_vertex = 3;
  uint32_t triangle_width = triangle_vertex * mesh.vertex_width[VertexBuffer::POSITION];
  bool has_hit = false;
  for (size_t i = 0; i < position.size / triangle_width; i++) {
    size_t base_index = triangle_width * i;
    glm::vec3 v0{ position.data[base_index], position.data[base_index + 1], position.data[base_index + 2] };
    glm::vec3 v1{ position.data[base_index + 3], position.data[base_index + 4], position.data[base_index + 5] };
    glm::vec3 v2{ position.data[base_index + 6], position.data[base_index + 7], position.data[base_index + 8] };
    float t;
    glm::vec3 barycentric;
    if (IntersectionEngine::TriangleIntersect(ray, v0, v1, v2, t, barycentric) &&
          t < mesh_t && t > 0) {
      has_hit = true;
      mesh_t = t;
      mesh_barycentric = barycentric;
      triangle_index = i;
    }
  }

  return has_hit;
}


} // namespace pathtracer 
