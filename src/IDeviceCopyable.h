#ifndef _CRT_IDEVICECOPYABLE_H_
#define _CRT_IDEVICECOPYABLE_H_

namespace pathtracer {

class RenderDevice;

class IDeviceCopyable {
public:
  virtual ~IDeviceCopyable() {}
  virtual void CopyToDevice(RenderDevice& device) const = 0;
private:
};

}

#endif // _CRT_IDEVICECOPYABLE_H_
