#ifndef CRT_ILOGGER_H_
#define CRT_ILOGGER_H_

#include <string>
#include <sstream>
#include <iomanip>

namespace pathtracer {

class ILogger {
public:
  enum LogLevel {
    DEBUG,
    INFO,
    LOG,
    WARNING,
    ERROR
  };

  virtual ~ILogger() {}
  virtual void Log( const std::string& str, LogLevel level = LogLevel::LOG) const = 0;
  virtual void LogReplace(const std::string & str, LogLevel level = LogLevel::LOG) const = 0;

  template <typename T>
  std::string FormatPrecision(T value, size_t n = 4) {
    std::ostringstream ss;
    ss << std::setprecision(n) << value;
    return ss.str();
  }

  std::string FormatTime(float seconds) {
    std::ostringstream ss;
    int32_t minutes = 0;
    if (seconds > 60.0f) {
      minutes = static_cast<int32_t>(seconds)/ 60;
      ss << minutes << " minutes ";
    }
    ss << FormatPrecision(seconds - (minutes * 60), 4) << " seconds.";
    return ss.str();
  }

private:
};

} // namespace pathtracer

#endif // CRT_ILOGGER_H_
