#include "ArgumentParser.h"
#include <regex>

ArgumentParser::ArgumentParser(int argc, char ** argv)
  : argc_{ argc },
    argv_{ argv } {
}

bool ArgumentParser::IsPresent(const std::string & arg) const {
  for (int32_t i = 0; i < argc_; i++) {
    if (argv_[i] == arg) {
      return true;
    }
  }
  return false;
}
// -l models/camera.dae models/sword27.obj -o skeleton_fight -w 1920 -h 1080
std::vector<std::string> ArgumentParser::GetArgList(const std::string & arg) const {
  std::vector<std::string> argument_list;
  bool found_arg = false;
  for (int32_t i = 0; i < argc_; i++) {
    if (argv_[i] == arg) {
      found_arg = true;
      continue;
    }    
    if (found_arg) {
      std::regex argument_regex(karg_regex_);
      if (std::regex_match(argv_[i], argument_regex)) {
        // We have hit a new argument. Break.
        break;
      }
      argument_list.push_back(argv_[i]);
    }
  }
  return argument_list;
}
