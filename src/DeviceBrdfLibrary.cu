#include "DeviceBrdfLibrary.h"

#include "CudaException.h"

namespace pathtracer {

template <class T, typename... Args>
__global__
void InitialisationKernel(T* memory, Args... args) {
  new(memory) T(args...);
}

template <class T, typename... Args>
inline void Initialise(T* ptr, Args&&... args) {
  InitialisationKernel<<<1, 1>>> (ptr, args...);
  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
}

DeviceBrdfLibrary::DeviceBrdfLibrary(RenderDevice & device) 
  : device_{ device } {
}

DeviceBrdfLibrary::~DeviceBrdfLibrary() {
  for (auto& device_brdf : brdf_memory_) {
    thrust::device_free(device_brdf);
  }
  for (auto& texture_object : textures_) {
    CHECK_CUDA_ERROR(cudaDestroyTextureObject(texture_object));
  }
  for (auto& texture_data : texture_data_) {
    CHECK_CUDA_ERROR(cudaDeviceSynchronize());
    CHECK_CUDA_ERROR(cudaFree(texture_data));
  }
}

// For now just lambert, but i intend to add other material types in the future.
void DeviceBrdfLibrary::AddMaterial(const Material& material){
  cudaTextureObject_t albedo_texture = 0;
  cudaTextureObject_t emissive_texture = 0;

  if (auto* texture = material.GetTexture(Material::ALBEDO)) {
    uint32_t index = this->FindTextureIndex(texture);
    albedo_texture = textures_[index];
  }
  if (auto* texture = material.GetTexture(Material::EMISSIVE)) {
    uint32_t index = this->FindTextureIndex(texture);
    emissive_texture = textures_[index];
  }

  device::MaterialComponent albedo(albedo_texture);
  device::MaterialComponent emissive(emissive_texture);

  // Allocate device memory and initialse object on GPU.
  thrust::device_ptr<device::LambertianBrdf> brdf_memory =
    thrust::device_malloc<device::LambertianBrdf>(1);
  Initialise<device::LambertianBrdf>(thrust::raw_pointer_cast(brdf_memory),
                                     albedo,
                                     emissive);
  // Add to device pointer list for use.
  uint32_t index = brdfs_.size();
  brdfs_.push_back(thrust::raw_pointer_cast(brdf_memory));

  host_index_to_device_map_.insert(std::make_pair(material.GetUuid(), index));
}

device::Brdf* DeviceBrdfLibrary::GetDeviceBrdf(uint32_t material_uuid) {
  uint32_t index = 0;
  auto found = host_index_to_device_map_.find(material_uuid);
  if (found != host_index_to_device_map_.end()) {
    index = found->second;
  }
  else {
    throw std::runtime_error("DeviceBrdfLibrary::GetDeviceMaterialId material "
                             "not found.");
  }
  return brdfs_[index];
}

uint32_t DeviceBrdfLibrary::FindTextureIndex(const Texture * texture) {
  uint32_t index;
  auto found = texture_to_index_map_.find(texture);
  if (found != texture_to_index_map_.end()) {
    index = found->second;
  }
  else {
    index = this->AddTexture(texture);
  }
  return index;
}

uint32_t DeviceBrdfLibrary::AddTexture(const Texture* texture) {
  // Allocate Texture. Create texture object.
  size_t pitch;
  float* texture_data = nullptr;
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
  CHECK_CUDA_ERROR(cudaMallocPitch(&texture_data,
                                   &pitch,
                                   texture->GetChannels() * sizeof(float) * texture->GetWidth(),
                                   texture->GetHeight()));
  texture_data_.push_back(texture_data);
  cudaMemcpy2D(texture_data, pitch, texture->GetData(),
               texture->GetChannels() * sizeof(float) * texture->GetWidth(),
               texture->GetChannels() * sizeof(float) * texture->GetWidth(),
               texture->GetHeight(), cudaMemcpyHostToDevice);
  // Resource descriptor.
  cudaResourceDesc resource = {};
  resource.resType = cudaResourceTypePitch2D;
  resource.res.pitch2D.devPtr = texture_data;
  resource.res.pitch2D.pitchInBytes = pitch;
  resource.res.pitch2D.width = texture->GetWidth();
  resource.res.pitch2D.height = texture->GetHeight();
  switch (texture->GetChannels()) {
    case 1: resource.res.pitch2D.desc = cudaCreateChannelDesc<float1>(); break;
    case 2: resource.res.pitch2D.desc = cudaCreateChannelDesc<float2>(); break;
    case 3: resource.res.pitch2D.desc = cudaCreateChannelDesc<float4>(); break;
    case 4: resource.res.pitch2D.desc = cudaCreateChannelDesc<float4>(); break;
  }
  // Texture object descriptor.
  cudaTextureObject_t texture_object;
  cudaTextureDesc tex_descriptor = {};
  tex_descriptor.readMode = cudaReadModeElementType;
  tex_descriptor.normalizedCoords = 1;
  tex_descriptor.addressMode[0] = cudaAddressModeClamp;
  tex_descriptor.addressMode[1] = cudaAddressModeClamp;
  CHECK_CUDA_ERROR(cudaCreateTextureObject(&texture_object, &resource, &tex_descriptor, nullptr));
  // Size of array before insert will give index of this texture object after insert.
  // the index is used by the material referring to this texture.
  size_t back_index = textures_.size();
  textures_.push_back(texture_object);
  texture_to_index_map_.insert(std::make_pair(texture, back_index));
  return back_index;
}

} // namespace pathtracer 
