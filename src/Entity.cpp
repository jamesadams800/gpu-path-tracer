#include "Entity.h"

namespace pathtracer {

Entity::Entity()
  : transform_matrix_{1}  {
}

void Entity::SetTranslation(const glm::vec3& vec) {
  transform_matrix_ = glm::translate(transform_matrix_, vec);
}

void Entity::SetTranslation(float x, float y, float z) {
  transform_matrix_ = glm::translate(transform_matrix_, { x, y, z });
}

void Entity::SetTransform(const glm::mat4 & mat) {
  transform_matrix_ = mat;
}

void Entity::SetRotation(const glm::vec3& axis, float rad) {
  transform_matrix_ = glm::rotate(transform_matrix_, rad, axis);
}

}