#include "Sphere.h"

#include "RenderDevice.h"
#include "DeviceScene.h"

namespace pathtracer {

Sphere::Sphere(float radius)
  : radius_{ radius },
    position_{ 0, 0, 0, 1} {
}

void Sphere::CopyToDevice(RenderDevice& device) const {
  device.GetScene().GetGeometry().AddSphere(*this);
}

} // namespace pathtracer
