#ifndef _CRT_RENDERSYSTEM_H_
#define _CRT_RENDERSYSTEM_H_

#include <vector>
#include <memory>
#include <chrono>

#include "ILogger.h"

namespace pathtracer {

class Scene;
class Framebuffer;
class RenderThread;
class SampleScheduler;
class AccumulatorThread;
class RenderDevice;

class RenderSystem {
public:

  struct Options {
    uint32_t light_bounces = 5;
    uint32_t ss_per_pixel = 1;
    uint32_t passes = 1;
  };

  explicit RenderSystem(ILogger& logger);
  ~RenderSystem();

  RenderSystem(RenderSystem&) = delete;
  RenderSystem& operator=(RenderSystem&) = delete;

  void Configure(const Options& options);
  void Render(const Scene& scene, Framebuffer& output);

private:
  void DisplayAvailableDevices() const;
  void SelectRenderMode();
  int32_t SelectDevice();

  ILogger& logger_;
  std::vector<int32_t> device_indices_;
  std::unique_ptr<SampleScheduler> scheduler_;
  std::unique_ptr<AccumulatorThread> accumulator_thread_;
  std::vector<std::unique_ptr<RenderDevice>> render_devices_;
  std::vector<std::unique_ptr<RenderThread>> render_threads_;
  Options options_;
};

} // namespace pathtracer 

#endif