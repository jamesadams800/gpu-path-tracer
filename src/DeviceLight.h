#ifndef CRT_DEVICELIGHT_H_
#define CRT_DEVICELIGHT_H_

#pragma warning(push, 0)
#include <glm\ext.hpp>
#include <glm\glm.hpp>
#pragma warning(pop)

#include "Ray.h"

namespace pathtracer {

namespace device {

struct SphereAreaLight {
  glm::vec4 position;
  float radius;
};

struct QuadAreaLight {
  glm::vec4 v0;
  glm::vec4 v1;
  glm::vec4 v2;
  glm::vec4 v3;
};

struct PointLight {
  glm::vec4 position;
};

constexpr bool IsSquare(size_t n, size_t r = 0) {
  return (r*r == n) || ((r*r < n) && IsSquare(n, r + 1));
}

struct Light {
  static constexpr uint32_t AREA_LIGHT_SAMPLES = 25;
  static_assert(IsSquare(AREA_LIGHT_SAMPLES), "AREA_LIGHT_SAMPLES must be perfect square" );
  enum LightType {
    POINT,
    QUAD_AREA,
    SPHERE_AREA
  };
  LightType type;
  float intensity;
  glm::vec3 color;
  union {
    QuadAreaLight quad_area;
    SphereAreaLight sphere_area;
    PointLight point;
  };
};

struct LightSample {
  glm::vec4 sample_point;
  device::Ray sample_ray;
};

} // namespace device

} // namespace pathtracer

#endif // CRT_DEVICELIGHT_H_