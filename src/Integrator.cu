#include "Integrator.h"

#include <thrust/host_vector.h>

#include "RenderDevice.h"
#include "RenderTarget.h"
#include "SampleScheduler.h"
#include "SimpleRayGenerator.h"
#include "NaiveIntersectionEngine.h"

namespace pathtracer {

Integrator::Integrator(RenderDevice& device)
  : device_{ device },
    light_bounces_{ 1 } {
}

Integrator::~Integrator() {
}

}