#ifndef CRT_REALISTICCAMERAINIT_H_
#define CRT_REALISTICCAMERAINIT_H_

#include "PinholeCamera.h"
#include "RealisticCamera.h"
#include "DeviceCamera.h"

#include <thrust/device_ptr.h>

namespace pathtracer {

class RenderDevice;

class DeviceCameraManager {
public:
  explicit DeviceCameraManager(RenderDevice&);
  ~DeviceCameraManager();

  void AddCamera(const PinholeCamera& camera);
  void AddCamera(const RealisticCamera& camera);

  thrust::device_ptr<device::Camera> GetCamera() {
    return camera_;
  }

  device::Camera::Type GetCameraType() const {
    return camera_type_;
  }

private:

  float FindFocus(const device::RealisticCamera& camera, float focus_distance) const;
  void CalculateThickLensApprox(const device::RealisticCamera& camera, 
                                glm::vec2& pz,
                                glm::vec2& fz) const;

  void CalculateExitPupil();

  RenderDevice & device_;
  device::Camera::Type camera_type_;
  thrust::device_ptr<device::Camera> camera_;
};

}

#endif // CRT_REALISTICCAMERAINIT_H_