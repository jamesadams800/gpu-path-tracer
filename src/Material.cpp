#include "Material.h"

#include "RenderDevice.h"
#include "DeviceScene.h"

namespace pathtracer {

uint32_t Material::uuid_counter_ = 0;

Material::Material()
  : uuid_{ uuid_counter_++},
    albedo_{0.0f,0.0f,0.0f},
    specular_ { 0.0f,0.0f,0.0f },
    emissive_ { 0.0f,0.0f,0.0f },
    is_emissive_{false},
    shininess_ {1.0f},
    textures_ {0} {
}

void Material::SetAlbedo(float r, float g, float b) {
  albedo_.r = r;
  albedo_.g = g;
  albedo_.b = b;
}

void Material::SetSpecular(float r, float g, float b) {
  specular_.r = r;
  specular_.g = g;
  specular_.b = b;
}

void Material::SetEmissive(float r, float g, float b) {
  emissive_.r = r;
  emissive_.g = g;
  emissive_.b = b;
  is_emissive_ = glm::dot(emissive_, emissive_) != 0.0f;
  this->SetTexture(std::unique_ptr<Texture>(new Texture(emissive_)), EMISSIVE);
}

void Material::SetShininess(float s) {
  shininess_ = s;
}

void Material::SetTexture(std::unique_ptr<Texture> texture, TextureType texture_index) {
  if (texture_index == EMISSIVE) {
    is_emissive_ = true;
  }
  textures_[texture_index] = std::move(texture);
}

void Material::CopyToDevice(RenderDevice & device) const {
  device.GetScene().GetBrdfs().AddMaterial(*this);
}

}