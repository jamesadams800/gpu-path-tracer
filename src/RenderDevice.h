#ifndef _CRT_RENDERDEVICE_H_
#define _CRT_RENDERDEVICE_H_

#include <stdint.h>

#include <cuda_runtime.h>
#include <thrust/device_allocator.h>

#include "Scene.h"

namespace pathtracer {

class DeviceScene;
class DeviceRng;

class RenderDevice {
public:
  explicit RenderDevice(int32_t device_index);
  ~RenderDevice();
  RenderDevice(const RenderDevice &) = delete;
  RenderDevice &operator=(const RenderDevice &) = delete;

  int32_t GetIndex() const {
    return device_index_;
  }

  void Bind();
  void DisplayDeviceProperties() const;
  bool HasBeenReset() const { return device_reset_; }
  void Reset() { device_reset_ = true; }

  int32_t GetThreadsPerBlock() const {
    return properties_.maxThreadsPerBlock;
  }

  size_t GetGlobalMemorySize() const {
    return properties_.totalGlobalMem;
  }

  std::pair<int32_t, int32_t> GetLaunchConfig(uint32_t elements_in_job) const {
    uint32_t threads = this->GetThreadsPerBlock();
    uint32_t blocks = (elements_in_job + threads - 1) / threads;
    return std::make_pair(blocks, threads);
  }

  DeviceScene& GetScene() {
    return *device_scene_;
  }

  DeviceRng& GetRng() {
    return *device_rng_;
  }

  void CopyToDevice(const Scene& scene);

private:
  int32_t device_index_;
  bool device_reset_;
  cudaDeviceProp properties_;
  std::unique_ptr<DeviceScene> device_scene_;
  std::unique_ptr<DeviceRng> device_rng_;
};

} // namespace pathtracer 

#endif