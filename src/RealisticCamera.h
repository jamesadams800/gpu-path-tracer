#ifndef CRT_REALISTICCAMERA_H_
#define CRT_REALISTICCAMERA_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "CameraBase.h"

namespace pathtracer {

struct Lens {
  bool is_stop;
  float radius;
  float thickness;
  float ri;
  float aperture;
};

class RealisticCamera : public CameraBase {
public:
  class LensBuilder;
  friend LensBuilder;

  explicit RealisticCamera(float aspect_ratio = 640.0f / 480.0f);
  void LookAt(float x, float y, float z) {
    this->LookAt(glm::vec3(x, y, z));
  }

  void LookAt(const glm::vec3& at) override {
    this->CameraBase::LookAt(at);
    focus_distance_ = glm::distance(this->GetAt(),
      glm::vec3(this->GetWorldTransform() * glm::vec4(this->GetEye(), 1.0f)));
  }

  void SetFilmSize(float size) {
    film_size_ = size;
  }

  float GetFilmSize() const {
    return film_size_;
  }

  float GetFocusDistance() const {
    return focus_distance_;
  }

  void RemoveLensSystem() {
    lens_system_.clear();
  }

  const std::vector<Lens>& GetLenses() const {
    return lens_system_;
  }

  void CopyToDevice(RenderDevice& device) const override;

private:
  float film_size_;
  float focus_distance_;
  std::vector<Lens> lens_system_;
};

class RealisticCamera::LensBuilder {
public:
  LensBuilder(RealisticCamera& camera) 
    : camera_{ camera } {
  }

  LensBuilder& AddLens(float radius, float thickness, float ri, float aperture) {
    Lens lens = {};
    lens.is_stop = false;
    lens.thickness = thickness;
    lens.radius = radius;
    lens.ri = ri;
    lens.aperture = aperture;
    camera_.lens_system_.push_back(lens);
    return *this;
  }

  LensBuilder& AddStop(float thickness, float aperture) {
    Lens lens = {};
    lens.is_stop = true;
    lens.thickness = thickness;
    lens.aperture = aperture;
    lens.ri = 1.0f;
    camera_.lens_system_.push_back(lens);
    return *this;
  }

private:
  RealisticCamera & camera_;
};

class LensSystemFactory {
public:
  static void AddDoubleGauss(RealisticCamera& camera);
  static void AddFishEye(RealisticCamera& camera);
  static void AddWideAngle(RealisticCamera& camera);
  static void AddTelephoto(RealisticCamera& camera);
};

} // namespace pathtracer

#endif // CRT_REALISTICCAMERA_H_