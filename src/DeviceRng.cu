#include "DeviceRng.h"

#include "RenderDevice.h"
#include "CudaException.h"

namespace pathtracer {

__global__ void InitCurandKernel(uint32_t seed, device::Array<device::Sampler> sampler) {
  uint32_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if (kernel_index < sampler.size) {
    new (&sampler.data[kernel_index]) device::Sampler(seed, kernel_index);
  }
}

DeviceRng::DeviceRng(RenderDevice& device)
  : DeviceRunnable(device) {
  this->InitialiseRandomSamplers(DEFAULT_RANDOM_SAMPLERS);
}

device::Array<device::Sampler> DeviceRng::GetSamplers(size_t required) {
  if (required > random_samplers_.size()) {
    std::cout << "DeviceRng::GetStates : Resizing Random states to " << required << std::endl;
    this->InitialiseRandomSamplers(required);
  }
  return device::ToArray(random_samplers_);
}

void DeviceRng::InitialiseRandomSamplers(size_t size) {
  random_samplers_.resize(size);
  auto config = this->GetDevice().GetLaunchConfig(size);
  InitCurandKernel<<<config.first, config.second>>>(time(0), device::ToArray(random_samplers_));
  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
}

__device__ device::Sampler::Sampler(uint32_t seed, uint32_t kernel_index) {
  curand_init((seed << 20) + kernel_index, 0, 0, &random_state_);
}

} // namespace pathtracer
