#ifndef _CRT_ACCUMULATORTHREAD_H_
#define _CRT_ACCUMULATORTHREAD_H_

#include "EventThread.h"

namespace pathtracer {

class AccumulatorBuffer;
class Framebuffer;

class AccumulatorThread : public EventThread {
public:
  explicit AccumulatorThread(ILogger&, const Framebuffer&);
  AccumulatorThread(const AccumulatorThread&) = delete;
  AccumulatorThread& operator=(AccumulatorThread&) = delete;

  bool AllSamplesHandled() const {
    return !this->HasUnhandledEvents() && this->IsWaiting();
  }

  void SendSamples(std::unique_ptr<thrust::host_vector<Sample>> samples);
  void SendWriteFramebuffer(Framebuffer* framebuffer);

private:
  void ThreadMain() override;
  void CheckThreadEvents(bool blocking);
  void AccumulateSamples(const thrust::host_vector<Sample>& samples);

  bool exit_;
  std::unique_ptr<AccumulatorBuffer> buffer_;
};

} // namespace pathtracer

#endif // 