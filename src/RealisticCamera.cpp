#include "RealisticCamera.h"

#include "RenderDevice.h"
#include "DeviceScene.h"

namespace pathtracer {

RealisticCamera::RealisticCamera(float aspect_ratio) 
  : film_size_{35.0f}, // 35 mm default film
    focus_distance_{ 0.0f }, 
    CameraBase(aspect_ratio) {
}

void RealisticCamera::CopyToDevice(RenderDevice & device) const {
  device.GetScene().GetCameraManager().AddCamera(*this);
}

void LensSystemFactory::AddDoubleGauss(RealisticCamera& camera) {
  camera.RemoveLensSystem();
  RealisticCamera::LensBuilder(camera).AddLens(29.475, 3.76, 1.67, 25.2)
                                      .AddLens(84.83, 0.12, 1, 25.2)
                                      .AddLens(19.275, 4.025, 1.67, 23)
                                      .AddLens(40.77, 3.275, 1.699, 23)
                                      .AddLens(12.75, 5.705, 1, 18)
                                      .AddStop(4.5, 17.1)
                                      .AddLens(-14.495, 1.18, 1.603, 17)
                                      .AddLens(40.77, 6.065, 1.658, 20)
                                      .AddLens(-20.385, 0.19, 1, 20)
                                      .AddLens(437.065, 3.22, 1.717, 20)
                                      .AddLens(-39.73, 0, 1, 20);
}

void LensSystemFactory::AddFishEye(RealisticCamera& camera) {
  camera.RemoveLensSystem();
  RealisticCamera::LensBuilder(camera).AddLens(30.2249, 0.8335, 1.62, 30.34)
                                      .AddLens(11.3931, 7.4136, 1, 20.68)
                                      .AddLens(75.2019, 1.0654, 1.639, 17.8)
                                      .AddLens(8.3349, 11.1549, 1, 13.42)
                                      .AddLens(9.5882, 2.0054, 1.654, 9.02)
                                      .AddLens(43.8677, 5.3895, 1, 8.14)
                                      .AddStop(1.4163, 6.08)
                                      .AddLens(29.4541, 2.1934, 1.517, 5.96)
                                      .AddLens(-5.2265, 0.9714, 1.805, 5.84)
                                      .AddLens(-14.2884, 0.0627, 1, 5.96)
                                      .AddLens(-22.3726, 0.94, 1.673, 5.96)
                                      .AddLens(-15.0404, 0, 1, 6.52);
}

void LensSystemFactory::AddWideAngle(RealisticCamera& camera) {
  camera.RemoveLensSystem();
      RealisticCamera::LensBuilder(camera).AddLens(35.98738, 1.21638, 1.54, 23.716)
                                          .AddLens(11.69718, 9.9957, 1, 17.996)
                                          .AddLens(13.08714, 5.12622, 1.772, 12.364)
                                          .AddLens(-22.63294, 1.76924, 1.617, 9.812)
                                          .AddLens(71.05802, 0.8184, 1, 9.152)
                                          .AddStop(2.27766, 8.756)
                                          .AddLens(-9.58584, 2.43254, 1.617, 8.184)
                                          .AddLens(-11.28864, 0.11506, 1, 9.152)
                                          .AddLens(-166.7765, 3.09606, 1.713, 10.648)
                                          .AddLens(-7.5911, 1.32682, 1.805, 11.44)
                                          .AddLens(-16.7662, 3.98068, 1, 12.276)
                                          .AddLens(-7.70286, 1.21638, 1.617, 13.42)
                                          .AddLens(-11.97328, 0, 1, 17.996);
}

void LensSystemFactory::AddTelephoto(RealisticCamera& camera) {
  camera.RemoveLensSystem();
  RealisticCamera::LensBuilder(camera).AddLens(54.6275, 12.52, 1.529, 47.5)
                                      .AddLens(-86.365, 3.755, 1.599, 44.5)
                                      .AddLens(271.7625, 2.8175, 1, 41.5)
                                      .AddStop(67.4125, 40.5)
                                      .AddLens(-32.13, 3.755, 1.613, 31.5)
                                      .AddLens(49.5325, 12.52, 1.603, 33.5)
                                      .AddLens(-50.945, 0, 1, 37);
}

} // namespace pathtracer 
