#ifndef _CRT_DEVICEGEOMETRY_H_
#define _CRT_DEVICEGEOMETRY_H_

#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#pragma warning(push, 0)
#include <glm\ext.hpp>
#include <glm\glm.hpp>
#pragma warning(pop)

#include "DeviceArray.h"
#include "DevicePrimitives.h"
#include "Mesh.h"
#include "Sphere.h"

namespace pathtracer {

namespace device {
  struct Geometry {
    device::Array<device::Sphere> spheres;
    device::Array<device::Mesh> meshes;
  };
}

class DeviceGeometry {
public:
  explicit DeviceGeometry(RenderDevice& device);
  ~DeviceGeometry();
  DeviceGeometry(const DeviceGeometry &) = delete;
  DeviceGeometry &operator=(DeviceGeometry &) = delete;

  thrust::device_ptr<device::Geometry> GetGeometry() {
    return device_geometry_;
  }

  const thrust::device_ptr<device::Geometry> GetGeometry() const {
    return device_geometry_;
  }

  thrust::device_ptr<device::Geometry> GetEmissiveGeometry() {
    return geometry_emitters_;
  }

  const thrust::device_ptr<device::Geometry> GetEmissiveGeometry() const {
    return geometry_emitters_;
  }

  void FinaliseGeometry();
  void AddSphere(const Sphere &sphere);
  void AddMesh(const Mesh &mesh);

private:
  void TransformMesh(device::Mesh* mesh, glm::mat4 transform, size_t size);
  void RebuildDescriptor();
  void CopyAttribute(const VertexBuffer &, const IndexBuffer &,
                     VertexBuffer::VertexAttribute, uint32_t attribute_size,
                     thrust::device_vector<float> &output);
  void BuildDeviceMeshes();

  struct MeshLoadData {
    // Material.
    glm::mat4 transform = {};
    bool is_emissive = false;
    device::Brdf* brdf = nullptr;
    size_t vertex_count = 0;
    std::pair<size_t, size_t> offsets[VertexBuffer::MAX_ATTRIBUTE_SIZE] = {};
    uint32_t attribute_width[VertexBuffer::MAX_ATTRIBUTE_SIZE] = {0};
  };

  RenderDevice& device_;
  thrust::device_ptr<device::Geometry> device_geometry_;
  thrust::device_vector<device::Sphere> spheres_;
  thrust::device_vector<device::Mesh> meshes_;

  thrust::device_ptr<device::Geometry> geometry_emitters_;
  thrust::device_vector<device::Mesh> mesh_emitters_;
  thrust::device_vector<device::Sphere> sphere_emitters_;
  // Mesh offsets used for building device mesh descriptors from device vector.
  std::vector<MeshLoadData> mesh_load_;
  thrust::device_vector<float> vertex_data_[VertexBuffer::MAX_ATTRIBUTE_SIZE];
};

} // namespace pathtracer

#endif // _CRT_DEVICEGEOMETRY_H_