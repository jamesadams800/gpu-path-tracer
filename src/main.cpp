#include "Application.h"

int main(int argc, char** argv) {  
  pathtracer::Application app(argc, argv);
  app.Run();
  return 0;
}