#include "EventThread.h"

#include <memory>

namespace pathtracer {

uint32_t EventThread::thread_index_counter_ = 0;

EventThread::EventThread(ILogger& logger)
: thread_index_ {thread_index_counter_++},
  waiting_{false},
  thread_{ nullptr },
  events_{},
  mutex_{},
  condition_{},
  logger_{ logger } {
}

EventThread::~EventThread() {
  if (thread_) {
    this->StopThread();
  }
}

void EventThread::StartThread() {
  thread_.reset(new std::thread(&EventThread::ThreadMain, this));
}

void EventThread::StopThread() {
  this->SendEvent(std::make_unique<ThreadEvent>(ThreadEvent::THREAD_TERMINATE_REQ));
  thread_->join();
  thread_.reset(nullptr);
}

void EventThread::SendEvent(std::unique_ptr<ThreadEvent> event) {
  std::unique_lock<std::mutex> lock(mutex_);
  events_.push(std::move(event));
  condition_.notify_one();
}

std::unique_ptr<ThreadEvent> EventThread::GetNextEvent(bool blocking) {
  std::unique_lock<std::mutex> lock(mutex_);
  while (blocking && events_.empty()) {
    waiting_ = true;
    condition_.wait(lock);
    waiting_ = false;
  }
  if (events_.empty()) {
    return nullptr;
  }
  else {
    auto event = std::move(events_.front());
    events_.pop();
    return std::move(event);
  }
}

}