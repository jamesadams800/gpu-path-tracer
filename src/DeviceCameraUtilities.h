#ifndef CRT_DEVICECAMERAUTILITIES_H_
#define CRT_DEVICECAMERAUTILITIES_H_

#include <cuda_runtime.h>
#include <curand_kernel.h>

#include "DeviceInterp.h"
#include "DeviceCamera.h"
#include "Ray.h"

namespace pathtracer {

// Realistic Camera Utilities for rendering with a realistic camera model.
// These utilities are closely adapted from the PBRT project. https://www.pbrt.org/
// 
class RealisticCameraUtilities {
public:

  __device__ __host__
  static inline device::Quad CalculateFilmQuad(const device::Camera& camera);

  __device__ __host__
  static inline glm::vec3 ScreenSpaceToFilmCoords(const device::Camera& camera,
                                                  const glm::vec2& ss_cords);

  __device__ __host__
  static inline glm::vec3 SampleExitPupil(const device::RealisticCamera& camera, 
                                          const glm::vec2& film_sample,
                                          const glm::vec2& states,
                                          float& exit_pupil_area);

  __device__ __host__
  static inline bool TraceLensSystemFromFilm(const device::RealisticCamera& camera,
                                             device::Ray& cumulative_ray);

  __device__ __host__
  static inline bool TraceLensSystemFromScene(const device::RealisticCamera& camera,
                                              device::Ray& cumulative_ray,
                                              uint32_t x = 0, uint32_t y = 0);

  __device__ __host__
  static inline bool IntersectSphericalLens(float radius,
                                            float z_center,
                                            const device::Ray& incident_ray,
                                            float& t,
                                            glm::vec3& normal);

  __device__ __host__
  static inline glm::vec2 CalculateCardinalPoints(const device::Ray& in, 
                                                  const device::Ray& out);

  __device__ __host__
  static inline device::Bounds FindExitPupilBounds(const device::RealisticCamera & camera,
                                                   const glm::vec2& points);

private:
  // Returns false if ray is internally refracted.
  __device__ __host__
    static inline bool Refract(const glm::vec3& normal,
                               float eta,
                               const glm::vec3& incident_ray,
                               glm::vec3& ray_direction);

  __device__ __host__
  static inline float RadicalInverse(float base, uint64_t a);

  __device__ __host__
  static inline uint64_t ReverseBits(uint64_t bits);

  __device__ __host__
  static inline uint32_t ReverseBits(uint32_t bits);
};

__device__ __host__
bool RealisticCameraUtilities::Refract(const glm::vec3& normal,
                                       float eta, 
                                       const glm::vec3& incident_ray, 
                                       glm::vec3& ray_direction) {

  float cos_theta_i = glm::dot(normal, incident_ray);
  float sin_sq_i = fmaxf(0.0f, 1.0f - cos_theta_i * cos_theta_i);
  float sin_theta_t = eta * eta * sin_sq_i;

  if (sin_theta_t >= 1.0f) {
    return false;
  }
  float cos_theta_t = sqrtf(1.0f - sin_theta_t);
  ray_direction = eta * -incident_ray + (eta * cos_theta_i - cos_theta_t) * normal;
  return true;
}

// These functions is modified from pbrt lowdiscrepancy.cpp with only the cases for base 0
// and 1. 

uint32_t RealisticCameraUtilities::ReverseBits(uint32_t n) {
  n = (n << 16) | (n >> 16);
  n = ((n & 0x00ff00ff) << 8) | ((n & 0xff00ff00) >> 8);
  n = ((n & 0x0f0f0f0f) << 4) | ((n & 0xf0f0f0f0) >> 4);
  n = ((n & 0x33333333) << 2) | ((n & 0xcccccccc) >> 2);
  n = ((n & 0x55555555) << 1) | ((n & 0xaaaaaaaa) >> 1);
  return n;
}

inline uint64_t RealisticCameraUtilities::ReverseBits(uint64_t n) {
  uint64_t n0 = ReverseBits((uint32_t)n);
  uint64_t n1 = ReverseBits((uint32_t)(n >> 32));
  return (n0 << 32) | n1;
}

inline float RealisticCameraUtilities::RadicalInverse(float base, uint64_t a) {
  if (base == 0) {
    return RealisticCameraUtilities::ReverseBits(a) * 5.4210108624275222e-20;
  }
  else if (base == 1) {
    constexpr float inv_base = 1.0f / 3.0f;
    uint64_t reversed = 0;
    float inv_base_n = 1;
    while (a) {
      uint64_t next = a / 3;
      uint64_t digit = a - next * 3;
      reversed = reversed * 3 + digit;
      inv_base_n *= inv_base;
      a = next;
    }
    assert(reversed * inv_base_n < 1.00001);
    return thrust::min(reversed * inv_base_n, 0.99999994f);
  }
}

__device__ __host__
device::Bounds RealisticCameraUtilities::FindExitPupilBounds(const device::RealisticCamera & camera,
                                                             const glm::vec2 & points) {
  device::Bounds pupil_bounds;
  pupil_bounds.max = { std::numeric_limits<float>::min(), std::numeric_limits<float>::min() };
  pupil_bounds.min = { std::numeric_limits<float>::max(), std::numeric_limits<float>::max() };
  constexpr size_t n_of_samples = 1024;// * 1024;
  uint32_t exiting_rays = 0;

  float rear_lens_radius = camera.lens_system[camera.lens_count - 1].aperture;
  device::Bounds projected_bounds;
  projected_bounds.min = glm::vec2(-1.5f * rear_lens_radius, -1.5f * rear_lens_radius);
  projected_bounds.max = glm::vec2(1.5f * rear_lens_radius, 1.5f * rear_lens_radius);

  for (uint32_t i = 0; i < n_of_samples; i++) {
    float ipoint = device::Interp::Linear((i + 0.5f) / n_of_samples, points.x, points.y);
    glm::vec3 film_point(ipoint, 0.0f, 0.0f);
    glm::vec2 u = { RealisticCameraUtilities::RadicalInverse(0, i), 
                    RealisticCameraUtilities::RadicalInverse(1, i) };
    glm::vec3 lens_point{
      device::Interp::Linear(u.x, projected_bounds.min.x, projected_bounds.max.x),
      device::Interp::Linear(u.y, projected_bounds.min.y, projected_bounds.max.y),
      camera.lens_back_z
    };

    device::Ray test_ray(film_point, glm::normalize(lens_point - film_point));
    if ((lens_point.x >= pupil_bounds.min.x && lens_point.x <= pupil_bounds.max.x &&
         lens_point.y >= pupil_bounds.min.y && lens_point.y <= pupil_bounds.max.y) ||
         RealisticCameraUtilities::TraceLensSystemFromFilm(camera, test_ray)) {
      pupil_bounds.min = { fminf(pupil_bounds.min.x, lens_point.x),
                           fminf(pupil_bounds.min.y, lens_point.y) };
      pupil_bounds.max = { fmaxf(pupil_bounds.max.x, lens_point.x),
                           fmaxf(pupil_bounds.max.y, lens_point.y) };
      exiting_rays++;
    }
  }


  // Return entire element bounds if no rays made it through the lens system
  if (exiting_rays == 0) {
    printf("RealisticCameraUtilities::FindExitPupilBounds :"
           " Unable to find exit pupil in x = [%f,%f] on film.\n", points.x, points.y);
    return projected_bounds;
  }

  float delta = 2 * glm::length(projected_bounds.max - projected_bounds.min) / sqrtf(n_of_samples);
  pupil_bounds.min = pupil_bounds.min - delta;
  pupil_bounds.max = pupil_bounds.max + delta;

  return pupil_bounds;
}

__device__ __host__ 
device::Quad RealisticCameraUtilities::CalculateFilmQuad(const device::Camera & camera) {
  device::Quad quad;

  quad.v0 = RealisticCameraUtilities::ScreenSpaceToFilmCoords(camera, { 1.0f, 1.0f });
  quad.v1 = RealisticCameraUtilities::ScreenSpaceToFilmCoords(camera, { 1.0f, 0.0f });
  quad.v2 = RealisticCameraUtilities::ScreenSpaceToFilmCoords(camera, { 0.0f, 0.0f });
  quad.v3 = RealisticCameraUtilities::ScreenSpaceToFilmCoords(camera, { 0.0f, 1.0f });

  return quad;
}

__device__ __host__
glm::vec3 RealisticCameraUtilities::ScreenSpaceToFilmCoords(const device::Camera& camera,
                                                            const glm::vec2& ss_cords) {
  glm::vec3 film_coords;
  float dev_x = (2 * ss_cords.x - 1);
  float dev_y = (2 * ss_cords.y - 1);
  film_coords.x = (camera.realistic.film_x * 0.5f) * dev_x;
  film_coords.y = (camera.realistic.film_y * 0.5f) * dev_y;
  film_coords.z = 0.0f;
  return film_coords;
}

__device__ __host__
glm::vec3 RealisticCameraUtilities::SampleExitPupil(const device::RealisticCamera& camera,
                                                    const glm::vec2& film_sample,
                                                    const glm::vec2& lens_sample,
                                                    float& exit_pupil_area) {
  glm::vec3 point;
  float film_centre_distance = sqrtf(film_sample.x * film_sample.x + film_sample.y * film_sample.y);
  size_t exit_pupil_index = film_centre_distance /
                            (camera.film_diag / 2) * device::RealisticCamera::EXIT_PUPIL_SAMPLES;
  exit_pupil_index = thrust::min(exit_pupil_index, device::RealisticCamera::EXIT_PUPIL_SAMPLES - 1);
  const device::Bounds& pupil_bounds = camera.pupil_bounds[exit_pupil_index];

  glm::vec2 diagonal = (pupil_bounds.max - pupil_bounds.min);
  exit_pupil_area = diagonal.x * diagonal.y;

  glm::vec2 lens_point;
  lens_point.x = device::Interp::Linear(lens_sample.x, pupil_bounds.min.x, pupil_bounds.max.x);
  lens_point.y = device::Interp::Linear(lens_sample.y, pupil_bounds.min.y, pupil_bounds.max.y);

  float sin_theta = (film_centre_distance != 0) ? film_sample.y / film_centre_distance : 0;
  float cos_theata = (film_centre_distance != 0) ? film_sample.x / film_centre_distance : 1;
  point = { cos_theata * lens_point.x - sin_theta * lens_point.y,
            sin_theta * lens_point.x + cos_theata * lens_point.y,
            camera.lens_back_z };

  return point;
}

__device__ __host__
bool RealisticCameraUtilities::TraceLensSystemFromFilm(const device::RealisticCamera& camera,
                                                       device::Ray& cumulative_ray) {
  float lens_z = 0.0f;
  for (int32_t i = camera.lens_count - 1; i >= 0; i--) {
    const device::Lens& current_lens = camera.lens_system[i];
    lens_z -= current_lens.thickness;

    float t;
    glm::vec3 lens_normal;
    if (current_lens.is_stop) {
      if (cumulative_ray.GetDirection().z >= 0.0) {
        return false;
      }
      t = (lens_z - cumulative_ray.GetOrigin().z) / cumulative_ray.GetDirection().z;
    }
    else {
      float z_center = lens_z + current_lens.radius;
      if (!RealisticCameraUtilities::IntersectSphericalLens(current_lens.radius,
                                                            z_center, 
                                                            cumulative_ray, 
                                                            t,
                                                            lens_normal)) {
        return false;
      }
    }
    if (t < 0) {
      return false;
    }

    glm::vec3 hit = cumulative_ray.GetOrigin() + cumulative_ray.GetDirection() * t;
    float r2 = hit.x * hit.x + hit.y * hit.y;
    if (r2 > current_lens.aperture * current_lens.aperture) {
      return false;
    }
    cumulative_ray.SetOrigin(hit);

    if (!current_lens.is_stop) {
      glm::vec3 refracted_direction;
      float etaI = current_lens.ri;
      float etaT;
      if (i == 0) {
        etaT = 1.0f;
      }
      else {
        etaT = camera.lens_system[i - 1].ri;
      }
      if (!RealisticCameraUtilities::Refract(lens_normal, 
                                             etaI / etaT, 
                                             -cumulative_ray.GetDirection(),
                                             refracted_direction)) {
        return false;
      }
      cumulative_ray.SetDirection(refracted_direction);
    }
  }
  return true;
}

__device__ __host__
bool RealisticCameraUtilities::TraceLensSystemFromScene(const device::RealisticCamera & camera, 
                                                        device::Ray & cumulative_ray,
                                                        uint32_t x, uint32_t y) {
  float lens_z = camera.lens_front_z;
  for (int32_t i = 0; i < camera.lens_count; i++) {
    const device::Lens& current_lens = camera.lens_system[i];
    float t;
    glm::vec3 lens_normal;
    if (current_lens.is_stop) {
      t = (lens_z - cumulative_ray.GetOrigin().z) / cumulative_ray.GetDirection().z;
    }
    else {
      float z_center = lens_z + current_lens.radius;
      if (!RealisticCameraUtilities::IntersectSphericalLens(current_lens.radius,
                                                            z_center,
                                                            cumulative_ray,
                                                            t,
                                                            lens_normal)) {
        return false;
      }
    }
    if (t < 0) {
      return false;
    }
    glm::vec3 hit = cumulative_ray.GetOrigin() + cumulative_ray.GetDirection() * t;
    float r2 = hit.x * hit.x + hit.y * hit.y;
    if (r2 > current_lens.aperture * current_lens.aperture) {
      return false;
    }
    cumulative_ray.SetOrigin(hit);
    if (!current_lens.is_stop) {
      glm::vec3 refracted_direction;
      float etaI = 1.0f;
      float etaT = current_lens.ri;
      if (i == 0) {
        etaI = 1.0f;
      }
      else {
        etaI = camera.lens_system[i - 1].ri;
      }
      if (!RealisticCameraUtilities::Refract(lens_normal,
                                             etaI / etaT,
                                             -cumulative_ray.GetDirection(),
                                             refracted_direction)) {
        return false;
      }
      cumulative_ray.SetDirection(refracted_direction);
    }
    lens_z += current_lens.thickness;
  }
  return true;
}

__device__ __host__
inline bool RealisticCameraUtilities::IntersectSphericalLens(float radius,
                                                             float z_center,
                                                             const device::Ray& incident_ray,
                                                             float & t,
                                                             glm::vec3 & normal) {
  glm::vec3 lens_z_direction = incident_ray.GetOrigin() - glm::vec3(0, 0, z_center);
  float a = incident_ray.GetDirection().x * incident_ray.GetDirection().x +
            incident_ray.GetDirection().y * incident_ray.GetDirection().y +
            incident_ray.GetDirection().z * incident_ray.GetDirection().z;
  float b = 2 * (incident_ray.GetDirection().x * lens_z_direction.x +
                 incident_ray.GetDirection().y * lens_z_direction.y +
                 incident_ray.GetDirection().z * lens_z_direction.z);
  float c = lens_z_direction.x * lens_z_direction.x +
            lens_z_direction.y * lens_z_direction.y +
            lens_z_direction.z * lens_z_direction.z -
            radius * radius;
  //printf("a %f, b %f, c %f \n", a, b, c);
  float discrim = (b * b) - (4 * a * c);
  //printf("discrim : %f \n", discrim);
  if (discrim < 0) {
    return false;
  }

  float root = sqrtf(discrim);
  float q;
  if (b < 0) {
    q = -.5 * (b - root);
  }
  else {
    q = -.5 * (b + root);
  }

  float t0 = q / a;
  float t1 = c / q;
  if (t0 > t1) {
    thrust::swap(t0, t1);
  }

  if ((incident_ray.GetDirection().z > 0) ^ (radius < 0)) {
    t = fminf(t0, t1);
  }
  else {
    t = fmaxf(t0, t1);
  }

  if (t < 0) {
    return false;
  }

  normal = glm::normalize(glm::vec3((lens_z_direction + t * incident_ray.GetDirection())));
  normal = glm::faceforward(normal, incident_ray.GetDirection(), normal);
  return true;
}

__device__ __host__
glm::vec2 RealisticCameraUtilities::CalculateCardinalPoints(const device::Ray & in,
                                                            const device::Ray & out) {
  glm::vec2 out_points;

  float tf = -out.GetOrigin().x / out.GetDirection().x;
  glm::vec3 out_t = out.GetOrigin() + tf * out.GetDirection();
  out_points.y = -out_t.z;

  float tp = (in.GetOrigin().x - out.GetOrigin().x) / out.GetDirection().x;
  out_t = out.GetOrigin() + tp * out.GetDirection();
  out_points.x = -out_t.z;

  return out_points;
}

} // namespace pathtracer 

#endif // CRT_DEVICECAMERAUTILITIES_H_