#include "SceneLoader.h"

#include <iostream>

#include <assimp\Importer.hpp>
#include <assimp\postprocess.h>
#include <assimp\scene.h>

#include "Texture.h"
#include "PointLight.h"

namespace pathtracer {

// Limit to 256 materials per file for now.
uint64_t SceneLoader::file_material_offset_ = 0;

SceneLoader::SceneLoader() 
  : material_count_{ 0 },
    material_global_file_index_to_scene_{} {
}

void SceneLoader::LoadScene(const std::string& file_name, Scene & output_scene) {
  Assimp::Importer importer;
  const aiScene* assimp_scene = importer.ReadFile(file_name,
                                                  aiProcess_Triangulate);
  if (!assimp_scene) {
    throw SceneLoadException("pathtracer::Scene::LoadScene :"
                             " Failed to load scene from file [" + std::string(file_name) +
                             "] : " + importer.GetErrorString());
  }

  file_material_offset_++;

  if (assimp_scene->HasMaterials()) {
    for (uint32_t i = 0; i < assimp_scene->mNumMaterials; i++) {
      material_count_++;

      std::unique_ptr<Material> material(BuildMaterial(output_scene, assimp_scene->mMaterials[i]));
      
      material_global_file_index_to_scene_.insert(
        std::make_pair(this->GetGlobalMaterialIndex(i), material->GetUuid())
      );
      output_scene.AddMaterial(std::move(material));
    }
  }

  if (assimp_scene->HasMeshes()) {
    for (uint32_t i = 0; i < assimp_scene->mNumMeshes; i++) {
      const aiMesh* assimp_mesh = assimp_scene->mMeshes[i];
      const aiNode* node = FindNode(assimp_scene->mRootNode, assimp_mesh->mName);
      output_scene.AddRenderable( BuildMesh(output_scene, assimp_mesh, node) );
    }
  }
}

uint64_t SceneLoader::GetGlobalMaterialIndex(uint32_t local_index) {
  assert(material_count_ < 256);
  return (file_material_offset_ << 8) + local_index;
}

const aiNode * SceneLoader::FindNode(const aiNode* node, const aiString & component_name) {
  if (node->mName == component_name) {
    return node;
  }
  for (uint32_t i = 0; i < node->mNumChildren; i++) {
    auto found_node = FindNode(node->mChildren[i], component_name);
    if (found_node != nullptr) {
      return found_node;
    }
  }
  return nullptr;
}


Mesh* SceneLoader::BuildMesh(Scene& scene, const aiMesh* assimp_mesh, const aiNode* node) {
  assert(assimp_mesh);

  std::unique_ptr<VertexBuffer> vertex_buffer( new VertexBuffer());
  std::vector<float> vertices;
  vertices.reserve(assimp_mesh->mNumVertices * 3);
  for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
    vertices.push_back(assimp_mesh->mVertices[i].x);
    vertices.push_back(assimp_mesh->mVertices[i].y);
    vertices.push_back(assimp_mesh->mVertices[i].z);
  }
  vertex_buffer->AddVertexAttributeData(VertexBuffer::POSITION, 3,
                                        assimp_mesh->mNumVertices,
                                        vertices.data());

  if (assimp_mesh->HasNormals()) {
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mNormals[i].x);
      vertices.push_back(assimp_mesh->mNormals[i].y);
      vertices.push_back(assimp_mesh->mNormals[i].z);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::NORMAL, 3,
                                          assimp_mesh->mNumVertices,
                                          vertices.data());
  }

  if (assimp_mesh->HasTangentsAndBitangents()) {
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mTangents[i].x);
      vertices.push_back(assimp_mesh->mTangents[i].y);
      vertices.push_back(assimp_mesh->mTangents[i].z);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TANGENT, 3,
                                          assimp_mesh->mNumVertices,
                                          vertices.data());
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mBitangents[i].x);
      vertices.push_back(assimp_mesh->mBitangents[i].y);
      vertices.push_back(assimp_mesh->mBitangents[i].z);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::BITANGENT, 3,
                                          assimp_mesh->mNumVertices,
                                          vertices.data());
  }

  if (assimp_mesh->HasTextureCoords(0)) {
    vertices.clear();
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(assimp_mesh->mTextureCoords[0][i].x);
      vertices.push_back(assimp_mesh->mTextureCoords[0][i].y);
    }
    // TODO: Fix this.
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD0, 2,
                                          assimp_mesh->mNumVertices,
                                          vertices.data());
  }
  else {
    vertices.clear();
    // Dummy tex coords used to read uniform textures generated.
    for (uint32_t i = 0; i < assimp_mesh->mNumVertices; i++) {
      vertices.push_back(0.5f);
      vertices.push_back(0.5f);
    }
    vertex_buffer->AddVertexAttributeData(VertexBuffer::TEXCOORD0, 2,
                                          assimp_mesh->mNumVertices,
                                          vertices.data());
  }

  // Index buffer population.
  std::unique_ptr<IndexBuffer> index_buffer(new IndexBuffer());
  std::vector<uint32_t> indices;
  indices.reserve(assimp_mesh->mNumFaces * 3);
  for (uint32_t i = 0; i < assimp_mesh->mNumFaces; i++) {
    for (uint32_t j = 0; j < assimp_mesh->mFaces[i].mNumIndices; j++) {
      indices.push_back(assimp_mesh->mFaces[i].mIndices[j]);
    }
  }
  index_buffer->AddIndexData(indices.data(), indices.size());
  
  std::unique_ptr<Mesh> mesh(new Mesh(std::move(vertex_buffer), std::move(index_buffer)));
  uint64_t file_material_id = this->GetGlobalMaterialIndex(assimp_mesh->mMaterialIndex);
  uint64_t material_id = material_global_file_index_to_scene_[file_material_id];
  if (const Material* material = scene.GetMaterial(material_id)) {
    mesh->SetMaterial(material);
  }
  else {
    throw SceneLoadException("pathtracer::Scene::LoadScene :"
                             " Failed to mesh. [" + std::string(assimp_mesh->mName.C_Str()) +
                             "] Missing material with file index" + 
                             std::to_string(file_material_id) + " and mapped uuid " +
                             std::to_string(material_id) );
  }
  

  if (node == nullptr) {
    std::cout << "Mesh Node \'" << assimp_mesh->mName.C_Str()
              << "\' is not contained in scene graph." << std::endl;
  }
  else {
    mesh->SetTransform(BuildMatrix(node->mTransformation));
  }

  return mesh.release();
}

Material* SceneLoader::BuildMaterial(Scene & scene, const aiMaterial* assimp_material) {
  std::unique_ptr<Material> material(new Material());
  aiColor3D color;
  aiString name;
  if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_NAME, name)) {
    //std::cout << "Material name = " << name.C_Str() << std::endl;
  }

  uint32_t texture_count = assimp_material->GetTextureCount(aiTextureType_DIFFUSE);
  if (texture_count == 1) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_DIFFUSE, 0, &path)) {
      material->SetTexture(std::make_unique<Texture>(path.C_Str()), Material::ALBEDO);
    }
  }
  else {
    if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_DIFFUSE, color)) {
      material->SetTexture(std::make_unique<Texture>(glm::vec3(color.r, color.g, color.b)), 
                           Material::ALBEDO);
    }
  }

  texture_count = assimp_material->GetTextureCount(aiTextureType_SPECULAR);
  if (texture_count == 1) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_SPECULAR, 0, &path)) {
      material->SetTexture(std::make_unique<Texture>(path.C_Str()), Material::SPECULAR);
    }
  }
  else {
    if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
      material->SetTexture(std::make_unique<Texture>(glm::vec3(color.r, color.g, color.b)),
                           Material::SPECULAR);
    }
  }

  texture_count = assimp_material->GetTextureCount(aiTextureType_EMISSIVE);
  if (texture_count == 1) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_EMISSIVE, 0, &path)) {
      material->SetTexture(std::make_unique<Texture>(path.C_Str()), Material::EMISSIVE);
    }
  }
  else {
    if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_EMISSIVE, color)) {
      if (color.r != 0.0f || color.g != 0.0f || color.b != 0.0f) {
        material->SetTexture(std::make_unique<Texture>(glm::vec3(color.r, color.g, color.b)),
                                                       Material::EMISSIVE);
      }
    }
  }

  texture_count = assimp_material->GetTextureCount(aiTextureType_SHININESS);
  if (texture_count == 1) {
    aiString path;
    if (aiReturn_SUCCESS == assimp_material->GetTexture(aiTextureType_SHININESS, 0, &path)) {
      material->SetTexture(std::make_unique<Texture>(path.C_Str()), Material::SHININESS);
    }
  }
  else {
    if (aiReturn_SUCCESS == assimp_material->Get(AI_MATKEY_COLOR_SPECULAR, color)) {
      material->SetTexture(std::make_unique<Texture>(glm::vec3(color.r, color.g, color.b)),
                                                     Material::SHININESS);
    }
  }

  return material.release();
}

glm::mat4 SceneLoader::BuildMatrix(const aiMatrix4x4& assimp_matrix) {
  glm::mat4 out_matrix;

  out_matrix[0][0] = assimp_matrix.a1;
  out_matrix[1][0] = assimp_matrix.a2;
  out_matrix[2][0] = assimp_matrix.a3;
  out_matrix[3][0] = assimp_matrix.a4;

  out_matrix[0][1] = assimp_matrix.b1;
  out_matrix[1][1] = assimp_matrix.b2;
  out_matrix[2][1] = assimp_matrix.b3;
  out_matrix[3][1] = assimp_matrix.b4;

  out_matrix[0][2] = assimp_matrix.c1;
  out_matrix[1][2] = assimp_matrix.c2;
  out_matrix[2][2] = assimp_matrix.c3;
  out_matrix[3][2] = assimp_matrix.c4;

  out_matrix[0][3] = assimp_matrix.d1;
  out_matrix[1][3] = assimp_matrix.d2;
  out_matrix[2][3] = assimp_matrix.d3;
  out_matrix[3][3] = assimp_matrix.d4;

  return out_matrix;
}

} // namespace pathtracer
