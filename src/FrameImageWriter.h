#ifndef _TIFFWRITER_H_
#define _TIFFWRITER_H_

#include <string>

#include "Framebuffer.h"

class FrameImageWriter {
public:
  enum Channel {
    COLOR,
    DEPTH
  };

   FrameImageWriter();
  ~FrameImageWriter();

  FrameImageWriter(const FrameImageWriter&) = delete;
  FrameImageWriter& operator=(const FrameImageWriter&) = delete;

  void Write(const std::string& file_name,
             const pathtracer::Framebuffer& framebuffer, 
             Channel channel);

private:
  void WriteColor(const std::string& file_name,
                  const pathtracer::Framebuffer& framebuffer);
  void WriteDepth(const std::string& file_name,
                  const pathtracer::Framebuffer& framebuffer);

  static constexpr uint32_t RGB_CHANNELS = 3;

  template <class T>
  T GetNormalisedInteger(float);

};

#endif // _TIFFWRITER_H_