#ifndef _CRT_INTERSECTION_H_
#define _CRT_INTERSECTION_H_

#pragma warning(push, 0)
#include <glm\ext.hpp>
#include <glm\glm.hpp>
#pragma warning(pop)

namespace pathtracer {

namespace device {

class Brdf;

struct Intersection {
  bool has_hit = false;
  float t = FLT_MAX;
  glm::vec3 position = { 0.0f,0.0f,0.0f };
  glm::vec3 normal = { 0.0f,0.0f,0.0f };
  glm::vec2 uv = { 0.0f,0.0f };
  device::Brdf* brdf = nullptr;
};

} // namespace device

} // namespace pathtracer

#endif // _CRT_INTERSECTION_H_