#ifndef CRT_DEVICEINTERPOLATOR_H_
#define CRT_DEVICEINTERPOLATOR_H_

#include "DevicePrimitives.h"

namespace pathtracer {

namespace device {

class Interp {
public:
  Interp() = delete;

  template <class T>
  __device__ __host__ static T Linear(float t, T a, T b) {
    return (1 - t) * a + t * b;
  }

  // Bilinear interpolation. v0 v1 v2 v3 are the points for interpolation laid out as: 
  // v0---v1
  // |    |
  // v2---v3
  // with x being the x interpolation factor and y being the y interpolation factor.
  template <class T>
  __device__ static T Bilinear(float x, float y, T& v0, T& v1, T& v2, T& v3) {
    T y1 = (1 - x) * v0 + x * v1;
    T y2 = (1 - x) * v2 + x * v3;
    return (1 - y) * y1 + y * y2;
  }

  template <class VEC_TYPE>
  __device__ static VEC_TYPE Barycentric(const VEC_TYPE& v0,
                                             const VEC_TYPE& v1,
                                             const VEC_TYPE& v2,
                                             const glm::vec3& uvw) {
    VEC_TYPE ret = (uvw.x * v0) +
                   (uvw.y * v1) +
                   (uvw.z * v2);
    return ret;
  }

  template <class VEC_TYPE>
  __device__ static VEC_TYPE BarycentricAttrib(const device::Mesh& mesh,
                                               VertexBuffer::VertexAttribute attribute,
                                               size_t triangle_index,
                                               const glm::vec3& barycentric_coefs) {
    size_t index = triangle_index * mesh.vertex_width[attribute] * 3;
    VEC_TYPE ret;
    if (index < mesh.vertex_data[attribute].size) {
      ret = device::Interp::Barycentric<VEC_TYPE>(&mesh.vertex_data[attribute].data[index],
                                                  barycentric_coefs);
    }
    return ret;
  }

  template <class VEC_TYPE>
  __device__ static VEC_TYPE Barycentric(float* ptr, const glm::vec3& uvw) {
    VEC_TYPE vectors[3];
    for (int32_t i = 0; i < 3; i++) {
      for (int32_t j = 0; j < VEC_TYPE::length(); j++) {
        vectors[i][j] = ptr[(VEC_TYPE::length() * i) + j];
      }
    }
    return Barycentric<VEC_TYPE>(vectors[0], vectors[1], vectors[2], uvw);
  }

private:
};

} // namespace device

} // namespace pathtracer 

#endif // CRT_DEVICEINTERPOLATOR_H_
