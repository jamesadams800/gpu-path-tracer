#include "Texture.h"

#include <cuda_texture_types.h>
#include <cudalibxt.h>
#include <cuda_runtime_api.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include <stb_image_resize.h>

namespace pathtracer {

struct StbDeleter {
  void operator()(float* data) {
    stbi_image_free(data);
  }
};

Texture::Texture()
  : texture_data_{ nullptr },
    channels_{0},
    width_{0},
    height_{0} {
}

Texture::Texture(const char * file_name)
  : Texture() {
  this->LoadFile(file_name);
}

Texture::Texture(const glm::vec3 & color)
  : Texture() {
  this->LoadUniformColor(color);
}

Texture::~Texture() {
}


void Texture::LoadFile(const char * file_name) {
  stbi_set_flip_vertically_on_load(true);
  texture_data_ = ImagePtr<float>(stbi_loadf(file_name, &width_, &height_, &channels_, 4),
                                  StbDeleter());
}

void Texture::LoadUniformColor(const glm::vec3 & color) {
  texture_data_ = ImagePtr<float>(new float[1 * 1 * 4],
                                  [](float* ptr){delete[] ptr;});
  width_ = 1;
  height_ = 1;
  channels_ = 4;
  for (uint32_t i = 0; i < width_; i++) {
    for (uint32_t j = 0; j < height_; j++) {
      uint32_t index = 4 * (i * width_ + j);
      texture_data_.get()[index] = color.r;
      texture_data_.get()[index + 1] = color.g;
      texture_data_.get()[index + 2] = color.b;
      texture_data_.get()[index + 3] = 1.0f;
    }
  }
}



} // namespace pathtracer