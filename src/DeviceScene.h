#ifndef CRT_DEVICESCENE_H_
#define CRT_DEVICESCENE_H_

#include <glm/glm.hpp>

#include "Scene.h"
#include "RealisticCamera.h"
#include "DeviceCamera.h"
#include "DeviceGeometry.h"
#include "DeviceBrdfLibrary.h"
#include "DeviceCameraManager.h"

namespace pathtracer {

class RenderDevice;

class DeviceScene {
public:
  explicit DeviceScene(RenderDevice& device);
  ~DeviceScene();
  DeviceScene(const DeviceScene &) = delete;
  DeviceScene &operator=(DeviceScene &) = delete;

  DeviceCameraManager& GetCameraManager() {
    return camera_manager_;
  }

  DeviceGeometry& GetGeometry() {
    return geometry_;
  }

  DeviceBrdfLibrary& GetBrdfs() {
    return brdfs_;
  }

private:
  RenderDevice& device_;
  DeviceCameraManager camera_manager_;
  DeviceGeometry geometry_;
  DeviceBrdfLibrary brdfs_;
};

} // namespace pathtracer

#endif // CRT_DEVICESCENE_H_