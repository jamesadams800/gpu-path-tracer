#include "RenderThread.h"

#include <unordered_map>
#include <assert.h>

#include "Scene.h"
#include "RenderSystem.h"
#include "SampleScheduler.h"
#include "AccumulatorThread.h"

#include "PathTracerIntegrator.h"

namespace pathtracer {

RenderThread::RenderThread(RenderDevice& device, ILogger& logger, uint32_t device_index)
  : exit_{ false },
    initialised_{false},
    accumulator_thread_{nullptr},
    scheduler_{nullptr},
    render_device_ { device },
    integrator_{ new PathTracerIntegrator(render_device_) },
    EventThread(logger) {
  Logger().Log("-----------------------------------------------------");
  Logger().Log("| Render Thread ID:" + std::to_string(this->GetId()) + " Device Index: " + 
                std::to_string(device_index) + "                 |");
  render_device_.DisplayDeviceProperties();
}


void RenderThread::CheckThreadEvents(bool blocking) {
  std::unique_ptr<ThreadEvent> event(this->GetNextEvent(blocking));
  if (event) {
    switch (event->id) {
      case ThreadEvent::THREAD_INIT_REQ: {
        Logger().Log("THREAD_INIT_REQ: Recieved in thread " + std::to_string(this->GetId()));
        ThreadEvent::RenderThreadInit& init_req = event->render_thread_init;
        this->ThreadInitialise(init_req.scene,
                               init_req.options,
                               init_req.accumulator,
                               init_req.scheduler);
        initialised_ = true;
        break;
      }
      case ThreadEvent::THREAD_TERMINATE_REQ: {
        // TODO: Sync logging.
        this->ThreadTerminate();
        Logger().Log("THREAD_TERMINATE_REQ: Recieved in thread " + std::to_string(this->GetId()));
        exit_ = true;
        break;
      }
      default: {
        Logger().Log("Unknown Event [" + std::to_string(event->id) + "] recieved in thread "
                      + std::to_string(this->GetId()), ILogger::ERROR);
        break;
      }
    }
  }
}

void RenderThread::SendInit(const Scene* scene,
                            const RenderSystem::Options* config,
                            AccumulatorThread* accumulator,
                            SampleScheduler* scheduler) {
  assert(scene && config && accumulator && scheduler);
  auto event = std::make_unique<ThreadEvent>(ThreadEvent::THREAD_INIT_REQ);
  ThreadEvent::RenderThreadInit& init_req = event->render_thread_init;
  init_req.scene = scene;
  init_req.options = config;
  init_req.accumulator = accumulator;
  init_req.scheduler = scheduler;
  this->SendEvent(std::move(event));
}

void RenderThread::ThreadMain() {
  Logger().Log("RenderThread::ThreadMain() :" + std::to_string(this->GetId()));
  render_device_.Bind();
  while(!exit_ && !initialised_) {
    // Await Initialisation.
    this->CheckThreadEvents(true);
  }
  if (initialised_) {
    this->MainLoop();
  }
}

void RenderThread::MainLoop() {
  Logger().Log("RenderThread::MainLoop() :" + std::to_string(this->GetId()));
  while(!exit_) {
    bool blocking = false;
    // Check for render work.
    if (scheduler_->HasScheduledSamples()) {
      SampleSpace sample_space;
      if (scheduler_->GetSample(sample_space)) {
        accumulator_thread_->SendSamples(integrator_->Render(sample_space));
      }
    }
    else {
      // Switch to blocking.
      blocking = true;
    }
    // Otherwise check for thread events.
    this->CheckThreadEvents(blocking);
  }
}

void RenderThread::ThreadInitialise(const Scene* scene,
                                    const RenderSystem::Options* config,
                                    AccumulatorThread* accumulator, 
                                    SampleScheduler* scheduler) {
  assert(scene && config && accumulator && scheduler);
  accumulator_thread_ = accumulator;
  scheduler_ = scheduler;
  render_device_.CopyToDevice(*scene);
  integrator_->SetLightBounces(config->light_bounces);
}

void RenderThread::ThreadTerminate() {

}

} // namespace pathtracer
