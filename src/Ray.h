#ifndef CRT_RAY_H_
#define CRT_RAY_H_

#include <stdint.h>
#include <vector>
#include <memory>
#include <math_constants.h>

#pragma warning(push, 0)
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#pragma warning(pop)

namespace pathtracer {

namespace device {

class Ray {
public:
  __host__ __device__ Ray() 
    : origin_{0.0f,0.0f,0.0f},
      direction_{0.0f,0.0f,1.0f},
      min_t_{ 0.0f },
      max_t_{ FP_INFINITE } {
  }
  __host__ __device__ explicit Ray(const glm::vec3& position, const glm::vec3& direction)
    : origin_{ position },
      direction_{ direction },
      min_t_{ 0.0f },
      max_t_{ FP_INFINITE } {
  }

  __host__ __device__ void SetOrigin(const glm::vec3& origin) {
    origin_ = origin;
  }

  __host__ __device__ void SetDirection(const glm::vec3& direction) {
    direction_ = direction;
  }

  __host__ __device__ const glm::vec3& GetOrigin() const {
    return origin_;
  }

  __host__ __device__ const glm::vec3& GetDirection() const {
    return direction_;
  }

  __host__ __device__ void SetMinT(float min) {
    min_t_ = min;
  }

  __host__ __device__ void SetMaxT(float max) {
    max_t_ = max;
  }

  __host__ __device__ float GetMinT() const {
    return min_t_;
  }

  __host__ __device__ float GetMaxT() const {
    return max_t_;
  }

private:
  float min_t_;
  float max_t_;
  glm::vec3 origin_;
  glm::vec3 direction_;
};

__device__ inline Ray operator*(const glm::mat4 transform, const Ray& ray) {
  return Ray( transform * glm::vec4(ray.GetOrigin(), 1.0f),
              glm::normalize(transform * glm::vec4(ray.GetDirection(), 0.0f)));
}


} // namespace device

} // namespace raytacer

#endif //CRT_RAY_H_