#ifndef _CRT_DEVICERUNNABLE_H_
#define _CRT_DEVICERUNNABLE_H_

namespace pathtracer {

class RenderDevice;

class DeviceRunnable {
public:
  explicit DeviceRunnable(RenderDevice& device) 
    : device_{ device } {
  }

protected:
  RenderDevice & GetDevice() {
    return device_;
  }
private:
  RenderDevice & device_;
};

} // namespace pathtracer

#endif // _CRT_DEVICERUNNABLE_H_
