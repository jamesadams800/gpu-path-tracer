#include "VertexBuffer.h"

#include <exception>

#include <cuda_runtime.h>

#include "CudaException.h"

namespace pathtracer {

VertexBuffer::VertexBuffer()
  : attribute_formats_{},
    vertex_data_{} {
}

VertexBuffer::~VertexBuffer() {
}

void VertexBuffer::AddVertexAttributeData(VertexAttribute attrib,
                                          uint32_t vertex_size,
                                          uint32_t vertex_count,
                                          float* data) {
  if (this->IsAttributePresent(attrib)) {
    throw std::runtime_error( std::string(__FUNCTION__ ) + ":" +
                              std::to_string(__LINE__) +  ": Attribute " +
                              std::to_string(attrib) + " already present.");
  }

  // Copy New attribute into new attribute array.
  VertexAttributeFormat new_attribute = {};
  new_attribute.vertex_attribute = attrib;
  new_attribute.vertex_offset = vertex_data_.size();
  new_attribute.vertex_size = vertex_size;
  new_attribute.vertex_count = vertex_count;

  attribute_formats_ .push_back(new_attribute);

  uint32_t total_vertex_size = vertex_size * vertex_count;
  vertex_data_.reserve(vertex_data_.size() + total_vertex_size);
  for (uint32_t i = 0; i < total_vertex_size; i++) {
    vertex_data_.push_back(data[i]);
  }
}

bool VertexBuffer::IsAttributePresent(VertexAttribute attribute_type) const noexcept {
  if (this->GetAttributeFormat(attribute_type)) {
    return true;
  }
  return false;
}

const VertexBuffer::VertexAttributeFormat* 
VertexBuffer::GetAttributeFormat(VertexAttribute attribute_type) const noexcept {
  for (auto& attribute_format : attribute_formats_) {
    if (attribute_format.vertex_attribute == attribute_type) {
      return &attribute_format;
    }
  }
  return nullptr;
}

VertexBuffer::Proxy VertexBuffer::operator[](VertexAttribute index) const {
  if (auto attribute = GetAttributeFormat(index)) {
    return Proxy(*this, *attribute);
  }
  else {
    throw std::runtime_error("VertexBuffer::operator[] : Attribute " + 
                             std::to_string(index) + " not present.");
  }
}


} // namespace pathtracer
