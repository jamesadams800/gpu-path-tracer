#ifndef _CRT_VERTEXBUFFER_H_
#define _CRT_VERTEXBUFFER_H_

#include <memory>
#include <vector>
#include <map>

namespace pathtracer {

class VertexBuffer {
public:

  enum VertexAttribute : uint32_t {
    POSITION,
    TANGENT,
    BITANGENT,
    NORMAL,
    COLOR,
    TEXCOORD0,
    TEXCOORD1,
    TEXCOORD2,
    TEXCOORD3,
    TEXCOORD4,
    TEXCOORD5,
    TEXCOORD6,
    TEXCOORD7,
    MAX_ATTRIBUTE_SIZE
  };

  struct VertexAttributeFormat {
    VertexAttribute vertex_attribute;
    uint32_t vertex_offset; // Index offset in data vector (For bytes * by float)
    uint32_t vertex_size; // Number of components per vertex attribute
    uint32_t vertex_count; // Number of elements in this attribute
  };

  struct Proxy {
  Proxy(const VertexBuffer& vertex_buffer,
        const VertexAttributeFormat& attribute)
    : vertex_buffer_{vertex_buffer}, attribute_format_{attribute} {}

  inline const float *operator[](uint32_t index) {
    return &vertex_buffer_.vertex_data_[attribute_format_.vertex_offset +
                                        attribute_format_.vertex_size * index];
  }

  private:
    const VertexBuffer &vertex_buffer_;
    const VertexAttributeFormat attribute_format_;
  };
    
  VertexBuffer();
  ~VertexBuffer();
  VertexBuffer(const VertexBuffer& other) = delete;
  VertexBuffer& operator=(const VertexBuffer& other) = delete;

  void AddVertexAttributeData(VertexAttribute attrib,
                              uint32_t vertex_size,
                              uint32_t vertex_count,
                              float* data );
  const std::vector<VertexAttributeFormat>& Attributes() const {
    return attribute_formats_;
  }

  bool IsAttributePresent(VertexAttribute attribute_type) const noexcept;
  const VertexAttributeFormat* GetAttributeFormat(VertexAttribute attribute_type) const noexcept;

  Proxy operator[](VertexAttribute index) const;

private:
  std::vector<VertexAttributeFormat> attribute_formats_;
  std::vector<float> vertex_data_;
};

} // namespace pathtracer

#endif _CRT_VERTEXBUFFER_H_ 