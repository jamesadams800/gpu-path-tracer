#include "DeviceBrdf.h"

#include <math_constants.h>

#include "Utilities.h"
#include "DeviceRng.h"

namespace pathtracer {

namespace device {

MaterialComponent::MaterialComponent(cudaTextureObject_t tex)
  : texture_{ tex } {
}

__device__ glm::vec4 MaterialComponent::Get(const glm::vec2& uv) const {
  if (texture_ != 0) {
    float4 lookup = tex2D<float4>(texture_, uv.x, 1 - uv.y);
    return glm::vec4(lookup.x, lookup.y, lookup.z, lookup.w);
  }
  else {
    return glm::vec4(0.0f);
  }
}

__device__ Brdf::Brdf(const MaterialComponent & emissive)
  : emissive_{ emissive } {
}

__device__ glm::vec3 Brdf::Le(const Intersection& intersection) const {
  float distance = fmax(fabs(intersection.t / 1000.0f), 1.0f);
  float distance_sq = 1.0f / (distance * distance);
  return distance_sq * emissive_.Get(intersection.uv);
}

__device__ LambertianBrdf::LambertianBrdf(const MaterialComponent& albedo,
                                          const MaterialComponent& emissive)
  : albedo_{ albedo },
    Brdf(emissive) {
}

__device__ float LambertianBrdf::Pdf(float cos_theta) const {
  return (1 / CUDART_PI_F) * cos_theta;
}
__device__ glm::vec3 LambertianBrdf::EvalDiff(const Intersection & intersection,
                                              const glm::vec3 & wo,
                                              const glm::vec3 & wi) const {
  glm::vec3 albedo = albedo_.Get(intersection.uv);
  return (1.0f / CUDART_PI_F) * albedo;
}


__device__ glm::vec3 LambertianBrdf::Evaluate(const Intersection& intersection,
                                              const glm::vec3& wo,
                                              const glm::vec3& wi) const {
  float cos_theta = glm::dot(intersection.normal, wi);
  glm::vec3 diff = this->EvalDiff(intersection, wo, wi);
  return (cos_theta * diff) / (1.0f / 2.0f * CUDART_PI_F);
}

__device__ glm::vec3 LambertianBrdf::SampleWi(device::Sampler& sampler,
                                              const Intersection& intersection,
                                              const glm::vec3& wo,
                                              glm::vec3& wi) const {
  glm::vec3 albedo = albedo_.Get(intersection.uv);
  glm::mat4 normal_coord = Utilities::CoordinateSystem(intersection.normal);
  wi = glm::normalize(normal_coord *
                      glm::vec4(this->DiffuseSample(sampler, intersection), 0.0f));
  float cos_theta = glm::dot(intersection.normal, wi);
  glm::vec3 brdf = (cos_theta * this->EvalDiff(intersection, wo, wi)) / this->Pdf(cos_theta);

  return brdf;
}

__device__ glm::vec3 LambertianBrdf::DiffuseSample(device::Sampler& sampler,
                                                   const Intersection& intersection) const {
  // Take diffuse sample from cosin weighted distribution.
  glm::vec2 u = sampler.UniformSample<glm::vec2>();
  float theta = sqrtf(1.0f - u.x);
  float phi = 2 * CUDART_PI_F * u.y;
  return glm::vec3( theta * cosf(phi),sqrtf(u.x), theta * sinf(phi));
}

} // namespace device 

} // namespace pathtracer
