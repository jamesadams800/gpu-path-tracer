#ifndef CRT_MESH_H_
#define CRT_MESH_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "Renderable.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

namespace pathtracer {

class Mesh : public Renderable {
public:
  Mesh(std::unique_ptr<const VertexBuffer>,
       std::unique_ptr<const IndexBuffer>);

  const VertexBuffer* GetVertexBuffer() const {
    return vertex_buffer_.get();
  }
  const IndexBuffer* GetIndexBuffer() const {
    return index_buffer_.get();
  }

  void CopyToDevice(RenderDevice& device) const override;

private:
  std::unique_ptr<const VertexBuffer> vertex_buffer_;
  std::unique_ptr<const IndexBuffer> index_buffer_;
};

} // namespace pathtracer 

#endif // CRT_MESH_H_