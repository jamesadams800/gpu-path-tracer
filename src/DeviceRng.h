#ifndef CRT_DEVICERNG_H_
#define CRT_DEVICERNG_H_

#include <curand.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <math_constants.h>

#pragma warning(push, 0)
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#pragma warning(pop)

#include "DeviceRunnable.h"
#include "DeviceArray.h"
#include "DeviceInterp.h"
#include "DeviceLight.h"
#include <stdio.h>

namespace pathtracer {

class RenderDevice;

namespace device {

class Sampler {
public:
  __device__ Sampler() : random_state_{ 0 } {}
  __device__ explicit Sampler(uint32_t seed, uint32_t kernel_index);

  __device__ inline glm::vec2 SampleConcentricDisk(float radius);

  __device__ inline glm::vec4 SampleConeSurface(float opening_angle);

  __device__ inline glm::vec4 SampleSphereSurface(float radius);

  __device__ inline glm::vec4 SampleHemisphere();

  __device__ inline uint32_t SampleUint();

  __device__ inline float SampleFloat();

  template <class VEC_TYPE>
  __device__ inline VEC_TYPE UniformSample();

private:
  curandState_t random_state_;
};

} // namespace device

class DeviceRng : public DeviceRunnable {
public:
  explicit DeviceRng(RenderDevice&);

  DeviceRng(const DeviceRng&) = delete;
  DeviceRng& operator=(const DeviceRng&) = delete;

  // required is the number of random states required by the calling kernel.
  device::Array<device::Sampler> GetSamplers(size_t required);
private:
  static constexpr size_t DEFAULT_RANDOM_SAMPLERS = 10000;

  void InitialiseRandomSamplers(size_t size);

  thrust::device_vector<device::Sampler> random_samplers_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// Random Kernel Utility functions. ///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace device {

__device__ glm::vec4 Sampler::SampleConeSurface(float opening_angle) {
  float r1 = 2.0f * CUDART_PI_F * curand_uniform(&random_state_);
  float x = sinf(opening_angle) * cosf(r1);
  float y = sinf(opening_angle) * sinf(r1);
  float z = cosf(opening_angle);
  return glm::vec4(x, y, z, 0.0f);
}

__device__
glm::vec4 Sampler::SampleSphereSurface(float radius) {
  float theta = 2 * CUDART_PI_F * curand_uniform(&random_state_);
  float phi = acos(1.0f - 2.0f * curand_uniform(&random_state_));
  float x = sin(phi) * cos(theta);
  float y = sin(phi) * sin(theta);
  float z = cos(phi);
  return radius * glm::vec4(x, y, z, 1.0f);
}

__device__ glm::vec4 Sampler::SampleHemisphere() {
  float r1 = curand_uniform(&random_state_);
  float r2 = curand_uniform(&random_state_);
  float sin_theta = sqrtf(1 - r1 * r1);
  float phi = 2 * CUDART_PI_F * r2;
  return glm::vec4(sin_theta * cosf(phi), r1, sin_theta * sinf(phi), 0.0f);
}

inline __device__ uint32_t Sampler::SampleUint() {
  return curand(&random_state_);
}

__device__ float Sampler::SampleFloat() {
  return curand_uniform(&random_state_);
}

__device__
glm::vec2 Sampler::SampleConcentricDisk(float radius) {
  glm::vec2 sample;
  float r1 = curand_uniform(&random_state_);
  float r2 = curand_uniform(&random_state_);
  float a = (2 * r1) - 1.0f;
  float b = (2 * r2) - 1.0f;
  float phi = 0;
  float mapped_radius;
  if (a * a > b * b) {
    mapped_radius = radius * a;
    phi = (CUDART_PI_F / 4.0f) * (b / a);
  }
  else {
    mapped_radius = radius * b;
    phi = (CUDART_PI_F / 2.0f) - ((CUDART_PI_F / 4.0f) * (a / b));
  }

  sample.x = cosf(phi) * mapped_radius;
  sample.y = sinf(phi) * mapped_radius;

  return sample;
}

template<class VEC_TYPE>
inline __device__ VEC_TYPE Sampler::UniformSample() {
  VEC_TYPE vector;
  for (int32_t i = 0; i < VEC_TYPE::length(); i++) {
    vector[i] = curand_uniform(&random_state_);
  }
  return vector;
}

} // namespace device

} // namespace pathtracer

#endif // CRT_DEVICERNG_H_