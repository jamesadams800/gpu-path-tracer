#include "PathTracerIntegrator.h"

#include <iostream>

#include "RenderDevice.h"
#include "RenderTarget.h"
#include "UniformSampleGenerator.h"
#include "RealisticCameraRayGenerator.h"
#include "SimpleRayGenerator.h"
#include "NaiveIntersectionEngine.h"
#include "CudaException.h"
#include "DeviceScene.h"


namespace pathtracer {

PathTracerIntegrator::PathTracerIntegrator(RenderDevice & device)
  : Integrator(device),
    sample_generator_{new UniformSampleGenerator(device)},
    ray_generator_{nullptr} {
}

__global__ void PathTrace(int32_t light_bounces,
                          device::Array<device::Sampler> samplers,
                          device::Array<Sample> samples,
                          device::Array<device::Ray> rays,
                          device::Array<float> weights,
                          const device::Geometry* geometry,
                          const device::Geometry* emitters) {
  size_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if (kernel_index < rays.size) {
    glm::vec3 illumination = glm::vec3(0.0f);
    glm::vec3 path_throughput = glm::vec3(1.0f);
    device::Sampler& sampler = samplers.data[kernel_index];
    device::Ray& current_ray = rays.data[kernel_index];

    for (uint32_t i = 0; i < light_bounces; i++) {
      device::Intersection intersection;
      NaiveIntersectionEngine::Intersect(current_ray,
                                         *geometry,
                                         intersection);
      if (!intersection.has_hit) {
        break;
      }
      illumination += path_throughput * intersection.brdf->Le(intersection);
      illumination += path_throughput * Integrator::DirectIllumination(sampler,
                                                                       *geometry,
                                                                       *emitters,
                                                                       current_ray,
                                                                       intersection);

      glm::vec3 wo = -current_ray.GetDirection();
      glm::vec3 wi;
      path_throughput = path_throughput * intersection.brdf->SampleWi(sampler,
                                                                      intersection, 
                                                                      wo,
                                                                      wi);

      current_ray.SetOrigin(intersection.position + (0.001f * intersection.normal));
      current_ray.SetDirection(wi);
    }
    samples.data[kernel_index].color = illumination * weights.data[kernel_index];
  }
}

std::unique_ptr<thrust::host_vector<Sample>> PathTracerIntegrator::Render(const SampleSpace & input) {
  DeviceScene& scene = device_.GetScene();
  if (ray_generator_ == nullptr) {
    if (scene.GetCameraManager().GetCameraType() == device::Camera::REALISTIC) {
      ray_generator_.reset(new RealisticCameraRayGenerator(device_));
    }
    else {
      ray_generator_.reset(new SimpleRayGenerator(device_));
    }
  }

  // Generate samples resizes samples_.
  uint32_t number_of_samples = sample_generator_->GenerateSamples(input, samples_);
  const auto& camera = device_.GetScene().GetCameraManager().GetCamera();
  const auto& geometry = device_.GetScene().GetGeometry();
  if (number_of_samples != rays_.size()) {
    this->ResizeCaches(number_of_samples);
  }

  ray_generator_->GeneratePrimaryRays(samples_, camera, rays_, weights_);

  auto config = device_.GetLaunchConfig(number_of_samples);
  PathTrace<<<config.first, config.second>>>(light_bounces_,
                                             device_.GetRng().GetSamplers(number_of_samples),
                                             device::ToArray(samples_),
                                             device::ToArray(rays_),
                                             device::ToArray(weights_),
                                             thrust::raw_pointer_cast(scene.GetGeometry().GetGeometry()),
                                             thrust::raw_pointer_cast(scene.GetGeometry().GetEmissiveGeometry()));

  return std::make_unique<thrust::host_vector<Sample>>(samples_);
}

// Generate samples resizes samples_. So we do not need to do it here.
void PathTracerIntegrator::ResizeCaches(size_t no_of_rays) {
  std::cout << "Resizing ray caches to " << no_of_rays << std::endl;
  rays_.resize(no_of_rays);
  weights_.resize(no_of_rays);
}

} // namespace pathtracer
