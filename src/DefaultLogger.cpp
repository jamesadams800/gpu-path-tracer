#include "DefaultLogger.h"

#include <iostream>
#include <assert.h>

namespace pathtracer {

DefaultLogger::DefaultLogger()
  : log_level_{ ILogger::LOG },
    stream_{ std::_Ptr_cout },
    lock_{} {
  assert(stream_);
}

void DefaultLogger::Log(const std::string & str, LogLevel level) const {
  if (level >= log_level_) {
    std::unique_lock<std::mutex> lock(lock_);
    *stream_ << str << std::endl;
  }
}

void DefaultLogger::LogReplace(const std::string & str, LogLevel level) const {
  if (level >= log_level_) {
    std::unique_lock<std::mutex> lock(lock_);
    *stream_ << "                                                     " << '\r' << std::flush;
    *stream_ << str << '\r' << std::flush;
  }
}


} // namespace pathtracer 
