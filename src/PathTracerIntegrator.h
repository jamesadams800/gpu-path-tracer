#ifndef CRT_PATHTRACERINTEGRATOR_H_
#define CRT_PATHTRACERINTEGRATOR_H_

#include "Integrator.h"
#include "Ray.h"

#include "Ray.h"
#include "Intersection.h"

namespace pathtracer {

class ShadingEngine;
class RealisticCameraRayGenerator;

class PathTracerIntegrator : public Integrator {
public:
  PathTracerIntegrator(RenderDevice&);

  std::unique_ptr<thrust::host_vector<Sample>> Render(const SampleSpace & input) override;

private:
  void ResizeCaches(size_t no_of_rays);

  std::unique_ptr<ISampleGenerator> sample_generator_;
  std::unique_ptr<IRayGenerator> ray_generator_;

  // Cached memory chunks used for tracing.  Stops reallocations every frame.
  thrust::device_vector<Sample> samples_;
  thrust::device_vector<device::Ray> rays_;
  thrust::device_vector<float> weights_;
};

} // namespace pathtracer

#endif // CRT_PATHTRACERINTEGRATOR_H_
