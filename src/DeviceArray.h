#ifndef CRT_DEVICEARRAY_H_
#define CRT_DEVICEARRAY_H_

#include <thrust/device_vector.h>

namespace pathtracer {

namespace device {

template <class T> struct Array {
  Array() = default;
  size_t size;
  T *data;
};

template <class T>
Array<T> ToArray(thrust::device_vector<T>& vector) {
  Array<T> array;
  array.size = vector.size();
  array.data = thrust::raw_pointer_cast(&vector[0]);
  return array;
}

template <class T>
const Array<const T> ToArray(const thrust::device_vector<T>& vector) {
  Array<const T> array;
  array.size = vector.size();
  array.data = thrust::raw_pointer_cast(&vector[0]);
  return array;
}

template <class T>
Array<T> ArrayFromOffsets(thrust::device_vector<T> &vector,
                          size_t start_offset,
                          size_t end_offset) {
  Array<T> array;
  array.size = end_offset - start_offset;
  array.data = thrust::raw_pointer_cast(&vector[start_offset]);
  return array;
}


} // namespace device 

} // namespace pathtracer

#endif // CRT_DEVICEARRAY_H_