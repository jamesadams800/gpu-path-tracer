#ifndef _CRT_DEVICEMATERIAL_H_
#define _CRT_DEVICEMATERIAL_H_

#include <cuda_runtime.h>
#include <thrust/device_vector.h>

#pragma warning(push, 0)
#include <glm\ext.hpp>
#include <glm\glm.hpp>
#pragma warning(pop)

#include "DeviceRng.h"
#include "Intersection.h"

namespace pathtracer {

namespace device {

class MaterialComponent {
public:
  __host__ explicit MaterialComponent(cudaTextureObject_t);
  __device__ glm::vec4 Get(const glm::vec2& uv) const;
private:
  cudaTextureObject_t texture_;
};

class Brdf {
public:
  __device__ explicit Brdf(const MaterialComponent& emissive);

  __device__ virtual glm::vec3 Le(const Intersection& intersection) const;

  __device__ virtual glm::vec3 Evaluate(const Intersection& intersection,
                                        const glm::vec3& wo,
                                        const glm::vec3& wi) const = 0;

  __device__ virtual glm::vec3 SampleWi(device::Sampler& sampler,
                                        const Intersection& intersection,
                                        const glm::vec3& wo,
                                        glm::vec3& wi ) const = 0;

protected:
  MaterialComponent emissive_;
};

class LambertianBrdf : public Brdf {
public:
  __device__ explicit LambertianBrdf(const MaterialComponent& albedo,
                                     const MaterialComponent& emissive);

  __device__ glm::vec3 Evaluate(const Intersection& intersection,
                                const glm::vec3& wo,
                                const glm::vec3& wi) const override;

  __device__ glm::vec3 SampleWi(device::Sampler& sampler,
                                const Intersection& intersection,
                                const glm::vec3& wo,
                                glm::vec3& wi) const override;
private:
  __device__ glm::vec3 EvalDiff(const Intersection& intersection,
                                const glm::vec3& wo,
                                const glm::vec3& wi) const;


  __device__ glm::vec3 DiffuseSample(device::Sampler& sampler,
                                     const Intersection& intersection) const;

  __device__ float Pdf(float cos_theta) const;

  MaterialComponent albedo_;
};

} // namespace device

} // namespace pathtracer

#endif  // _CRT_DEVICEMATERIAL_H_