#ifndef _CRT_SIMPLERAYGENERATOR_H_
#define _CRT_SIMPLERAYGENERATOR_H_

#include "DeviceRunnable.h"
#include "IRayGenerator.h"

namespace pathtracer {

class RenderDevice;

class SimpleRayGenerator : public DeviceRunnable, public IRayGenerator {
public:
  explicit SimpleRayGenerator(RenderDevice& device);
  void GeneratePrimaryRays(const thrust::device_vector<Sample>& samples,
                           const thrust::device_ptr<device::Camera>,
                           thrust::device_vector<device::Ray>& primary_rays,
                           thrust::device_vector<float>& weights) override;

  __host__ __device__ static device::Ray GenPrimaryRay(device::Camera*, 
                                                       glm::vec2 screen_space_coord);

protected:
};

} // namespace pathtracer

#endif // _CRT_SIMPLERAYGENERATOR_H_

