#include "RealisticCameraRayGenerator.h"

#include <random>

#include "CudaException.h"
#include "RenderDevice.h"
#include "RenderTarget.h"
#include "DeviceCameraUtilities.h"

namespace pathtracer {

__global__ void RealisticPrimaryRayGenKernel(const device::Array<const Sample> samples,
                                             device::Array<device::Sampler> samplers,
                                             device::Camera* camera,
                                             device::Array<device::Ray> rays,
                                             device::Array<float> weights) {
  uint32_t kernel_index = blockIdx.x * blockDim.x + threadIdx.x;
  if (kernel_index < rays.size) {
    glm::vec2 ss_coord = samples.data[kernel_index].ss_cord;
    device::Ray& primary_ray = rays.data[kernel_index];
    weights.data[kernel_index] = RealisticCameraRayGenerator::GenPrimaryRay(camera,
                                                                            samplers.data[kernel_index],
                                                                            ss_coord,
                                                                            primary_ray);
  }
}

RealisticCameraRayGenerator::RealisticCameraRayGenerator(RenderDevice & device)
  : DeviceRunnable(device) {
}

void RealisticCameraRayGenerator::GeneratePrimaryRays(const thrust::device_vector<Sample>& samples,
                                                      const thrust::device_ptr<device::Camera> camera,
                                                      thrust::device_vector<device::Ray>& rays,
                                                      thrust::device_vector<float>& weights) {
  assert(rays.size() == samples.size() && rays.size() == weights.size());
  auto samplers = this->GetDevice().GetRng().GetSamplers(rays.size());
  auto config = this->GetDevice().GetLaunchConfig(rays.size());
  RealisticPrimaryRayGenKernel<<<config.first, config.second>>>(device::ToArray(samples),
                                                                samplers,
                                                                thrust::raw_pointer_cast(camera),
                                                                device::ToArray(rays),
                                                                device::ToArray(weights));
  CHECK_CUDA_ERROR(cudaGetLastError());
  CHECK_CUDA_ERROR(cudaDeviceSynchronize());
}

__device__
float RealisticCameraRayGenerator::GenPrimaryRay(device::Camera * camera,
                                                 device::Sampler& sampler,
                                                 const glm::vec2& ss_cord,
                                                 device::Ray& primary_ray) {
  uint32_t lens_index = camera->realistic.lens_count - 1;
  const device::Lens& back_lens = camera->realistic.lens_system[lens_index];
  glm::vec3 film_sample = RealisticCameraUtilities::ScreenSpaceToFilmCoords(*camera, ss_cord);
  // Invert film coords to flip image.
  film_sample.x = -film_sample.x;
  glm::vec2 lens_sample = sampler.UniformSample<glm::vec2>();
  float exit_pupil_area;
  glm::vec3 lens_point = RealisticCameraUtilities::SampleExitPupil(camera->realistic, 
                                                                   film_sample, 
                                                                   lens_sample, 
                                                                   exit_pupil_area);

  device::Ray cumulative_ray(film_sample, glm::normalize(lens_point - film_sample));

  // Calaulate cos^4(theta) weighting term. Modify to address issue:
  // https://github.com/mmp/pbrt-v3/issues/162 to bring into account the exit pupil size.
  float weight = cumulative_ray.GetDirection().z * cumulative_ray.GetDirection().z;
  weight = weight * weight;
  glm::vec2 diagonal = (camera->realistic.pupil_bounds[0].max - camera->realistic.pupil_bounds[0].min);
  weight = 2.0f * weight * exit_pupil_area / (diagonal.x * diagonal.y);

  if (!RealisticCameraUtilities::TraceLensSystemFromFilm(camera->realistic,
                                                         cumulative_ray)) {
    primary_ray = device::Ray(glm::vec3(0.0f), glm::vec3(0.0f));
    return 0.0f;
  }

  primary_ray = camera->camera_to_world * cumulative_ray;

  return weight;
}

} // namespace pathtracer