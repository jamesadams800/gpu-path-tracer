#ifndef CRT_LIGHT_H_
#define CRT_LIGHT_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "Entity.h"
#include "IDeviceCopyable.h"

namespace pathtracer {

class Light : public Entity {
public:
  Light()
    : Light({ 1.0f,1.0f,1.0f }, 1.0f ) {
  }

  explicit Light(const glm::vec3& color, float intensity = 1.0f)
    : light_color_{ color },
      light_intensity_{ intensity } {
  }

  glm::vec3 GetColor() const {
    return light_color_;
  }

  void SetIntensity(float intensity) {
    light_intensity_ = intensity;
  }

  float GetIntensity() const {
    return light_intensity_;
  }

protected:
  glm::vec3 light_color_;
  float light_intensity_;
};

} // namespace pathtracer 

#endif // CRT_LIGHT_H_