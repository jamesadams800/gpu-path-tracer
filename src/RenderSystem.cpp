#include "RenderSystem.h"

#include "Framebuffer.h"
#include "CudaException.h"
#include "SampleScheduler.h"
#include "AccumulatorBuffer.h"
#include "AccumulatorThread.h"
#include "RenderThread.h"
#include "Scene.h"
#include "Integrator.h"
#include "Timer.h"

namespace pathtracer {

RenderSystem::RenderSystem(ILogger& logger)
  : logger_{logger},
    scheduler_{nullptr},
    accumulator_thread_{nullptr},
    render_devices_{},
    render_threads_{} {
  int32_t device_count;
  CHECK_CUDA_ERROR(cudaGetDeviceCount(&device_count));
  for (int32_t i = 0; i < device_count; i++) {
    device_indices_.push_back(i);
  }
}

RenderSystem::~RenderSystem() {
}

void RenderSystem::Configure(const Options & options) {
  options_ = options;
}

void RenderSystem::Render(const Scene & scene, Framebuffer& framebuffer) {
  Timer timer;
  // Allocate Threads.
  scheduler_.reset(new SampleScheduler(logger_, framebuffer));
  scheduler_->SetPasses(options_.passes);
  scheduler_->SetSuperSamples(options_.ss_per_pixel);

  for (auto index : device_indices_) {
    render_devices_.emplace_back( new RenderDevice(index) );
  }

  logger_.Log("RenderSystem::Render: Initialising Render Threads.");
  accumulator_thread_.reset(new AccumulatorThread(logger_, framebuffer));
  for (auto& device : render_devices_ ) {
    render_threads_.emplace_back( new RenderThread(*device, logger_, device->GetIndex()));
  }
  // Start Threads.
  accumulator_thread_->StartThread();
  for (auto& render_thread : render_threads_) {
    render_thread->StartThread();
  }
  // Initialise Threads.
  scheduler_->GenerateSampleSpaces();
  for (auto& render_thread : render_threads_) {
    render_thread->SendInit(&scene, &options_, accumulator_thread_.get(), scheduler_.get());
  }

  timer.Start();
  // GO!
  logger_.Log("RenderSystem::Render: Render Threads started. Waiting for completion.");
  // Wait for result. I'll stick in a Condtional variable in here soon....
  bool done = false;
  while (!done) {
    if (!scheduler_->HasScheduledSamples() && accumulator_thread_->AllSamplesHandled()) {
      done = true;
    }
    for (auto& thread : render_threads_) {
      if (!thread->IsWaiting()) {
        done = false;
      }
    }
  }
  timer.Stop();
  logger_.Log("RenderSystem::Render: Render completed.  Copying results to framebuffer.");

  logger_.Log("RenderSystem::Render: Render duration = " + logger_.FormatTime(timer.GetTimeS()));
  // Copy Accumulator to framebuffer.
  accumulator_thread_->SendWriteFramebuffer(&framebuffer);
  for (;;) {
    if (!accumulator_thread_->HasUnhandledEvents() && accumulator_thread_->IsWaiting()) {
      break;
    }
  }

  // Stop threads.
  accumulator_thread_->StopThread();
  for (auto& render_thread : render_threads_) {
    render_thread->StopThread();
  }
  render_threads_.clear();
}

void RenderSystem::DisplayAvailableDevices() const {
  int32_t device_count;
  CHECK_CUDA_ERROR(cudaGetDeviceCount(&device_count));
  for (int32_t i = 0; i < device_count; i++) {
    cudaDeviceProp cudaprops;
    CHECK_CUDA_ERROR(cudaGetDeviceProperties(&cudaprops, i));
    std::cout << i << ": " << cudaprops.name << std::endl;
  }
}

void RenderSystem::SelectRenderMode() {
  std::cout << "GPU Devices currently selected for rendering [";
  for (auto index : device_indices_) {
    std::cout << index << ",";
  }
  std::cout << "]" << std::endl;

  std::cout << "Options" << std::endl;
  std::cout << "-------" << std::endl;
  std::cout << "a : Greedy mode. Select all devices (WARNING: this "
               "can cause issues if your OS display driver uses hardware acceleration)." << std::endl;
  std::cout << "n : Add NEW device." << std::endl;
  std::cout << "d : DELETE a selected device." << std::endl;
  std::cout << "s : Start render." << std::endl;
  std::cout << "q : Quit." << std::endl;
  for (;;) {
    char selection;
    if (std::cin >> selection) {
      switch (selection) {
      case 'a': break;
      case 'n': break;
      case 'd': break;
      case 's': break;
      case 'q': break;
      }
    }
    else {
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
  }
}

int32_t RenderSystem::SelectDevice() {
  int32_t device_index = 0;
  int32_t device_count;
  CHECK_CUDA_ERROR(cudaGetDeviceCount(&device_count));
  if (device_count == 0) {
    throw std::runtime_error("No CUDA devices available.");
  }

  for (;;) {
    std::cout << "Select Device for use:" << std::endl;
    this->DisplayAvailableDevices();
    if (std::cin >> device_index && (device_index < device_count && device_index >= 0)) {
      break;
    }
    else {
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
  }

  std::cout << "Selected device " << device_index << std::endl;
  return device_index;
}

} // namespace pathtracer 
