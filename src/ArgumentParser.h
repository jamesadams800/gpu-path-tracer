#ifndef ARG_PARSER_H_
#define ARG_PARSER_H_

#include <stdint.h>
#include <vector>
#include <sstream>

class ArgumentParser {
public:
  ArgumentParser(int argc, char ** argv);

  template <class T>
  bool Get(const std::string& arg, T& out) const {
    auto string_vec = this->GetArgList(arg);
    if (string_vec.size() > 0) {
      std::stringstream ss(string_vec[0]);
      ss >> out;
      return true;
    }
    return false;
  }
  bool IsPresent(const std::string& arg) const;
  std::vector<std::string> GetArgList(const std::string& arg) const;

private:
  constexpr static const char* karg_regex_ = "^[-]{1}\\w{1}$";
  int32_t argc_;
  char ** argv_;
};

#endif
