#ifndef CRT_DEVICEBRDFLIBRARY_H_
#define CRT_DEVICEBRDFLIBRARY_H_

#include <unordered_map>

#include <thrust/device_vector.h>

#include "Material.h"
#include "DeviceBrdf.h"
#include "DeviceArray.h"

namespace pathtracer {

class RenderDevice;

class DeviceBrdfLibrary {
public:
  DeviceBrdfLibrary(RenderDevice& device);
  ~DeviceBrdfLibrary();
  DeviceBrdfLibrary(DeviceBrdfLibrary&) = delete;
  DeviceBrdfLibrary& operator=(DeviceBrdfLibrary&) = delete;

  void AddMaterial(const Material& material);
  device::Brdf* GetDeviceBrdf(uint32_t host_material_id);

  thrust::device_vector<device::Brdf*>& GetBrdfs() {
    return brdfs_;
  }

private:  
  uint32_t FindTextureIndex(const Texture* texture);
  uint32_t AddTexture(const Texture* texture);

  RenderDevice& device_;
  std::unordered_map<uint32_t, uint32_t> host_index_to_device_map_;
  thrust::host_vector<thrust::device_ptr<device::Brdf>> brdf_memory_;
  thrust::device_vector<device::Brdf*> brdfs_;
  // Textures.
  // Host vector to hold device allocations for textures.  As CUDA textures
  // are referred to by their cudaTextureObject_t in kernel code the underlying
  // texture memory pointers do nto need to be kept on the device.
  thrust::host_vector<float*> texture_data_;
  std::unordered_map<const Texture*, uint32_t> texture_to_index_map_;
  thrust::device_vector<cudaTextureObject_t> textures_;
};

} // namespace pathtracer

#endif // CRT_DEVICEBRDFLIBRARY_H_