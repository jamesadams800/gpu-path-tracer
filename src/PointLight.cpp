#include "PointLight.h"

#include "RenderDevice.h"
#include "Sphere.h"

namespace pathtracer {

PointLight::PointLight(const glm::vec3& position,
                       const glm::vec3& color,
                       float intensity)
  : position_{ position },
    Light(color, intensity) {
}

PointLight::PointLight(const glm::vec3& color, float intensity)
  : PointLight({ 0.0f,0.0f,0.0f }, color, intensity) {
}

} // namespace pathtracer
