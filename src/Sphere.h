#ifndef _CRT_SPHERE_H_
#define _CRT_SPHERE_H_

#include "Renderable.h"

namespace pathtracer {

class Sphere : public Renderable {
public:
  explicit Sphere(float radius);

  void SetRadius(float radius) {
    radius_ = radius;
  }

  float GetRadius() const { 
    return radius_;
  }

  glm::vec4 GetCentre() const {
    return position_;
  }

  void CopyToDevice(RenderDevice& device) const override;

private:
  float radius_;
  glm::vec4 position_;
};

} // namespace pathtracer

#endif // _CRT_SPHERE_H_