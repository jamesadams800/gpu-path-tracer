#ifndef CRT_PINHOLECAMERA_H_
#define CRT_PINHOLECAMERA_H_

#include <stdint.h>
#include <vector>
#include <memory>

#include "CameraBase.h"

namespace pathtracer {

class PinholeCamera : public CameraBase {
public:
  explicit PinholeCamera(float fov = 90, float aspect_ratio = 640.0f/480.0f);

  void SetFov(float fov) {
    fov_ = fov;
  }

  float GetFov() const {
    return fov_;
  }

  float GetNearPlane() const {
    return near_plane_;
  }

  void CopyToDevice(RenderDevice& device) const override;

protected:
private:
  float near_plane_;
  float fov_;
};

} // namespace pathtracer

#endif // CRT_PINHOLECAMERA_H_