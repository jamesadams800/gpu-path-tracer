#include "DeviceScene.h"

namespace pathtracer {

DeviceScene::DeviceScene(RenderDevice & device) 
  : device_{ device },
    camera_manager_{ device },
    geometry_{ device },
    brdfs_{ device } {
}

DeviceScene::~DeviceScene() {
}

} // namespace pathtracer
