#ifndef _CRT_SCENE_H_
#define _CRT_SCENE_H_

#include <stdexcept>
#include <functional>
#include <vector>
#include <memory>
#include <unordered_map>
#include <assert.h>

#include "IDeviceCopyable.h"

namespace pathtracer {

class SphereAreaLight;
class CameraBase;
class Renderable;
class Texture;
class Material;

class Scene : public IDeviceCopyable {
public:
  Scene();
  ~Scene();

  void AddLight(SphereAreaLight* light);
  void AddCamera(CameraBase* camera);
  void AddRenderable(Renderable* renderable);

  void AddMaterial(std::unique_ptr<Material> material);

  const Material* GetMaterial(uint32_t id);

  void CopyToDevice(RenderDevice & device) const override;

private:
  std::vector<std::unique_ptr<CameraBase>> cameras_;
  std::vector<std::unique_ptr<Renderable>> renderables_;

  std::unordered_map< uint32_t, std::unique_ptr<Material> > material_library_;
};

} // namespace pathtracer 

#endif // _CRT_SCENE_H_
