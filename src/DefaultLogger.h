#ifndef CRT_DEFAULTLOGGER_H_
#define CRT_DEFAULTLOGGER_H_

#include "ILogger.h"
#include <mutex>
#include <ostream>

namespace pathtracer {

class DefaultLogger : public ILogger {
public:
  // Logger defaults to std::cout.  Use SetStream to set underlying stream.
  // logger does not take ownership of said stream.
  DefaultLogger();

  void SetLogLevel(ILogger::LogLevel log_level) {
    log_level_ = log_level;
  }

  void SetStream(std::ostream* ostream) {
    stream_ = ostream;
  }

  void Log(const std::string & str, LogLevel level = LogLevel::LOG) const override;
  void LogReplace(const std::string & str, LogLevel level) const override;

private:
  ILogger::LogLevel log_level_;
  std::ostream* stream_;
  mutable std::mutex lock_;
};

} // namespace pathtracer

#endif // CRT_DEFAULTLOGGER_H_