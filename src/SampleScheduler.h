#ifndef CRT_SAMPLESCHEDULER_H_
#define CRT_SAMPLESCHEDULER_H_

#include <stdint.h>
#include <queue>
#include <mutex>
#include <iostream>

#include "ILogger.h"

namespace pathtracer {

class Framebuffer;

struct SampleSpace {
  // Resolution
  uint32_t res_x;
  uint32_t res_y;
  // Top left coords of sample space.
  uint32_t start_x;
  uint32_t start_y;
  // Bottom right coords of sample space.
  uint32_t end_x;
  uint32_t end_y;
  // Super sampling per pixel kernel size.
  // the pixel will be split into an NxN 
  // grid with samples taken at even intervals.
  uint32_t pixel_ss_kernel_size;

  friend std::ostream& operator<<(std::ostream& os, const SampleSpace& dt);
};

class SampleScheduler {
public:
  explicit SampleScheduler(ILogger& logger, const Framebuffer& framebuffer);

  void SetPasses(uint32_t pass) {
    number_of_passes_ = pass;
  }

  void SetSuperSamples(uint32_t samples) {
    super_samples_kernel_size_ = samples;
  }

  bool IsContinuous() const {
    return continuous_rendering_;
  }
  void SetContinuousRendering(bool val) {
    continuous_rendering_ = val;
  }

  bool HasScheduledSamples() const {
    std::unique_lock<std::mutex> lock(queue_mutex_);
    return !sample_queue_.empty();
  }

  bool GetSample(SampleSpace& sample);
  void GenerateSampleSpaces();

private:
  ILogger & logger_;
  bool continuous_rendering_;
  uint32_t number_of_passes_;
  uint32_t super_samples_kernel_size_;
  uint32_t buffer_x_res_;
  uint32_t buffer_y_res_;
  uint32_t max_samples_per_space_;
  mutable std::mutex queue_mutex_;
  std::queue<SampleSpace> sample_queue_;
  size_t queue_max_size_;
};

} // namespace pathtracer

#endif // CRT_SAMPLESCHEDULER_H_
