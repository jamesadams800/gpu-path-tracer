#include "PinholeCamera.h"

#include <math.h>
#include <assert.h>
#include <math_constants.h>

#include "RenderDevice.h"
#include "DeviceScene.h"

namespace pathtracer {

PinholeCamera::PinholeCamera(float fov, float aspect_ratio)
  : near_plane_{0.1f},
    fov_{ fov },
    CameraBase{ aspect_ratio } {
}

void PinholeCamera::CopyToDevice(RenderDevice& device) const {
  device.GetScene().GetCameraManager().AddCamera(*this);
}

}